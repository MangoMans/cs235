#include "Players.hpp"


/* PLAYER CHARACTER ****************************************************/
/***********************************************************************/

PlayerCharacter::PlayerCharacter( string name )
    : Character( name )
{
    m_str = 2;
    m_armor = 14;
    m_maxHp = 12;
    m_hp = 12;
    m_weaponName = "Greatsword"; // placeholder
}

string PlayerCharacter::GetAction()
{
    int userChoice;
    cout << "1. Attack     or     2. Defend" << endl;
    cin >> userChoice;
    string actionName = Character::GetAction( userChoice );
    return "actionName"; // placeholder
}

int PlayerCharacter::GetDamageRoll()
{
    int die1 = rand()% 6 + 1;
    int die2 = rand()% 6 + 1;
    return die1 + die2 + 2;
}

/* NON-PLAYER CHARACTER ************************************************/
/***********************************************************************/

NonPlayerCharacter::NonPlayerCharacter( string name )
    : Character( name )
{
    m_str = 2;
    m_armor = 116;
    m_maxHp = 27;
    m_hp = m_maxHp;
    m_weaponName = "Morningstar";
}

string NonPlayerCharacter::GetAction()
{
    int random = rand() % 2 + 1;
    string actionName = Character::GetAction( random );
    return "actionName"; // placeholder
}

int NonPlayerCharacter::GetDamageRoll()
{
    int die1 = rand() % 8 + 1;
    int die2 = rand() % 8 + 1;
    return die1 + die2 + 2; // placeholder
}

/* CHARACTER (parent class) FUNCTIONS **********************************/
/***********************************************************************/

Character::Character()
{
    m_healSlots = 2;
}

Character::Character( string name )
{
    m_healSlots = 2;
    SetName( name );
}

string Character::GetAction( int choice )
{
    if      ( choice == 1 ) { return "attack"; }
    else if ( choice == 2 ) { return "heal"; }
    else                    { return "unknown"; }
}

int Character::GetHP()
{
    return m_hp;
}

int Character::GetArmor()
{
    return m_armor;
}

string Character::GetName()
{
    return m_name;
}

void Character::SetName( string name )
{
    m_name = name;
}

int Character::GetHitRoll()
{
    int die = rand() % 20 + 1;
    cout << m_name << " attacks with their " << m_weaponName << "!" << endl;
    return die + m_str;
}

int Character::GetDamageRoll()
{
    return 0;
}

int Character::Heal()
{
    if ( m_healSlots <= 0 )
    {
        cout << "Out of heal slots!" << endl;
        return 0;
    }

    int die = rand() % 8 + 1;
    m_healSlots--;
    ChangeHP( die );
    cout << m_name << " casts Cure Wounds and heals " << die << " hp!" << endl;
    return die;
}

void Character::TakeDamage( int hitRoll, int damageRoll )
{
    if ( hitRoll >= m_armor )
    {
        cout << m_name << " takes " << damageRoll << " damage!" << endl;
        ChangeHP( -damageRoll );
    }
    else
    {
        cout << "The attack misses..." << endl;
    }
}

void Character::ChangeHP( int amount )
{
    m_hp += amount;
    if ( m_hp <= 0 )
    {
        m_hp = 0;
        cout << m_name << " falls unconscious!" << endl;
    }
    else if ( m_hp > m_maxHp ) { m_hp = m_maxHp; }
}
