#ifndef _GAME_PROGRAM_HPP
#define _GAME_PROGRAM_HPP

#include "Players.hpp"

#include <iomanip>
using namespace std;

class GameProgram
{
    public:
    void Run()
    {
        cout << endl << endl << "----------- GAME -----------" << endl;
        string name;
        cout << "Enter a name: ";
        cin >> name;

        PlayerCharacter player( name );
        NonPlayerCharacter npc( "Bugbear" );
        string playerAction, npcAction;

        int round = 0;
        while ( player.GetHP() > 0 && npc.GetHP() > 0 )
        {
            round++;
            cout << endl << "----- Round " << round << "-----" << endl;

            cout << left << setw(20)
                << player.GetName()
                << player.GetHP() << " hp, "
                << player.GetArmor() << " ac"
                << endl;
            cout << left << setw(20)
                << npc.GetName()
                << npc.GetHP() << " hp, "
                << npc.GetArmor() << " ac"
                << endl << endl;

            // Player turn
            playerAction = player.GetAction();
            cout << endl << player.GetName() << "'s turn..." << endl;

            if ( playerAction == "attack" )
            {
                int hitRoll = player.GetHitRoll();
                cout << "(hit roll:     " << hitRoll << ")" << endl;
                int damageRoll = player.GetDamageRoll();
                cout << "(damage roll:  " << damageRoll << ")" << endl;
                npc.TakeDamage( hitRoll, damageRoll );
            }
            else if ( playerAction == "heal" )
            {
                player.Heal();
            }

            // Enemy turn
            npcAction = npc.GetAction();
            cout << endl << npc.GetName() << "'s turn..." << endl;

            if ( npcAction == "attack" )
            {
                int hitRoll = npc.GetHitRoll();
                cout << "(hit roll:     " << hitRoll << ")" << endl;
                int damageRoll = npc.GetDamageRoll();
                cout << "(damage roll:  " << damageRoll << ")" << endl;
                player.TakeDamage( hitRoll, damageRoll );
            }
            else if ( npcAction == "heal" )
            {
                npc.Heal();
            }
        }
    }
};

#endif
