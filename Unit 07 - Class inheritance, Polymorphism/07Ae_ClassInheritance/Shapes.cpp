#include "Shapes.hpp"


/* RECTANGLE ***********************************************************/
/***********************************************************************/

void Rectangle::Setup( float width, float height, string unit )
{
    Shape::Setup( unit );
    m_width = width;
    m_height = height;
}

void Rectangle::DisplayArea()
{
    CalculateArea();

    cout << CalculateArea() << m_unit;
}

float Rectangle::CalculateArea()
{
    return m_width * m_height;
}

/* CIRCLE **************************************************************/
/***********************************************************************/

void Circle::Setup( float radius, string unit )
{
}

void Circle::DisplayArea()
{
}

float Circle::CalculateArea()
{
    return 0; // placeholder
}

/* SHAPE (parent class) FUNCTIONS **************************************/
/***********************************************************************/

void Shape::Setup( string unit )
{
    m_unit = unit;
}

void Shape::DisplayArea()
{
    cout << " " << m_unit << "^2";
}

float Shape::CalculateArea()
{
    return 0;
}
