#include <iostream>
using namespace std;

#include "File.hpp"

int main()
{
    File fileA;
    fileA.Create( "myfile", "txt" );
    fileA.AddContents( "Hello, world!" );
    fileA.AddContents( "Goodbye, world!" );

    cout << endl << "FILE A:" << endl;

    fileA.DisplayInfo();
    fileA.DisplayContents();

    // Copy some files


    return 0;
}
