#include "Character.hpp"

#include <cstdlib>
#include <iostream>
using namespace std;

/* CHARACTER *******************************************************/
Character::Character( string name )
    : MAX_HP( 100 )
{
    m_name = name;
    m_strength = rand() % 5 + 5;
    m_defense = rand() % 3 + 2;
    m_hp = MAX_HP;
}

string Character::GetName()
{
    return m_name;
}

int Character::GetHP()
{
    return m_hp;
}

int Character::GetStrength()
{
    return m_strength;
}

int Character::GetDefense()
{
    return m_defense;
}

string Character::GetType()
{
    return m_type;
}

void Character::Heal( int amount )
{
    m_hp += amount;
    if ( m_hp > MAX_HP )
    {
        m_hp = MAX_HP;
    }
}

void Character::GetAttacked( int damage )
{
    damage -= m_defense;
    m_hp -= damage;

    if ( m_hp < 0 )
    {
        m_hp = 0;
    }
}

void Character::DisplayInfo()
{
    cout << "NAME: " << m_name << "\t HP:" << m_hp << "/" << MAX_HP << endl;
    cout << "STR:  " << m_strength << "\t DEF: " << m_defense << endl;
}

bool Character::IsDead()
{
    return (m_hp == 0);
}

/* NPC CHARACTER ***************************************************/

NonPlayerCharacter::NonPlayerCharacter( string name )
    : Character( name )
{
    m_type = "NPC";
}

int NonPlayerCharacter::Action()
{
    return rand() % 2 + 1; // TEMP
}

int NonPlayerCharacter::AttackWho( int maxIndex )
{
    AttackWho( rand() % maxIndex );
    return 0; // TEMP
}

/* PC CHARACTER ****************************************************/

PlayerCharacter::PlayerCharacter( string name )
    : Character( name )
{
    m_type = "PC";
}

int PlayerCharacter::Action()
{
    int choice;
    cout << "Chose an action:" << endl;
    cout << "1. Attack" << endl;
    cout << "2. Heal" << endl;
    cin >> choice;

    while (choice != 1 && choice != 2 )
    {
        cout << "Invalid choice. Try again." << endl;
        cin >> choice;
    }

    return choice;

}

int PlayerCharacter::AttackWho( int maxIndex )
{
    int choice;
    cout << "Attack who? Chose 0 to " << maxIndex << ": ";
    cin >> choice;

    return choice;
}
