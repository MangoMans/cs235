#include "BattleProgram.hpp"
#include "Menu.hpp"
#include "StringUtil.hpp"

#include <ctime>
#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

BattleProgram::BattleProgram()
{
    Setup();
}

BattleProgram::~BattleProgram()
{
    Cleanup();
}

void BattleProgram::Run()
{
    while ( !m_done )
    {
        m_round++;
        Menu::Header( "ROUND " + StringUtil::ToString( m_round ) );

        // Display stats for all characters
        DisplayStats();

        // Each character's turn
        // Check if only one player survives
        int survivingPlayers = 0;
        for ( auto& character : m_characters )
        {
            cout << character->GetName() << "'s turn... ";
            if ( character->IsDead() )
            {
                cout << "They're dead! Skipping..." << endl;
                continue;
            }
            else
            {
                survivingPlayers++;
            }

            int action = character->Action();

            if ( action == 1 )      // Attack
            {
                int damage = character->GetStrength();
                int attackWho = character->AttackWho( m_characters.size() - 1 );

                if ( character->GetType() == "NPC" )
                {
                    cout << "They attack "
                         << m_characters[ attackWho ]->GetName()
                         << " for " << damage << " damage!" << endl;
                 }
                 else
                 {
                    cout << endl << "* " << character->GetName() << " attacks "
                         << m_characters[ attackWho ]->GetName()
                         << " for " << damage << " damage!" << endl;
                 }

                m_characters[ attackWho ]->GetAttacked( damage );
            }
            else if ( action == 2 ) // Heal
            {
                int healAmount = rand() % 5 + 1;

                if ( character->GetType() == "NPC" )
                {
                    cout << "They heal themself for " << healAmount << " hp!" << endl;
                 }
                 else
                 {
                    cout << endl << "* " << character->GetName() << " heals themself for "
                         << healAmount << " hp!" << endl;
                 }

                character->Heal( healAmount );
            }
        }

        if ( survivingPlayers == 1 )
        {
            cout << endl << endl;
            Menu::DrawHorizontalBar( 80, '!' );
            cout << "!! ONE CHARACTER LEFT STANDING! !! " << endl;
            Menu::DrawHorizontalBar( 80, '!' );

            m_done = true;
        }

        Menu::Pause();
    }

    Menu::DrawHorizontalBar( 80, '#' );
    Menu::DrawHorizontalBar( 80, '#' );
    Menu::DrawHorizontalBar( 80, '#' );
    Menu::DrawHorizontalBar( 80, '#' );
    Menu::DrawHorizontalBar( 80, '#' );

    cout << endl << "BATTLE RESULT:" << endl;

    // Display stats for all characters
    DisplayStats();
}

void BattleProgram::DisplayStats()
{
    cout << left
        << setw( 10 ) << "NAME"
        << setw( 10 ) << "TYPE"
        << setw( 10 ) << "HP"
        << setw( 10 ) << "STRENGTH"
        << setw( 10 ) << "DEFENSE"
        << setw( 20 ) << "CHARACTER #"
        << endl;
    Menu::DrawHorizontalBar( 80, '-' );
    for ( unsigned int i = 0; i < m_characters.size(); i++ )
    {
        cout << left
            << setw( 10 ) << m_characters[i]->GetName()
            << setw( 10 ) << m_characters[i]->GetType()
            << setw( 10 ) << m_characters[i]->GetHP()
            << setw( 10 ) << m_characters[i]->GetStrength()
            << setw( 10 ) << m_characters[i]->GetDefense()
            << setw( 20 ) << i
            << endl;
    }
    cout << endl;
}

void BattleProgram::Setup()
{
    srand( time( NULL ) );
    m_done = false;
    m_round = 0;

    cout << "How many players? ";
    int totalPlayers;
    cin >> totalPlayers;

    for ( int i = 0; i < totalPlayers; i++ )
    {
        cout << endl;
        cout << "Player " << i << ", (1) NPC or (2) PC? ";
        int choice;
        cin >> choice;

        cout << "Player " << i << " name: ";
        string name;
        cin >> name;

        Character* character = nullptr;
        if ( choice == 1 )
        {
            character = new NonPlayerCharacter( name );
        }
        else if ( choice == 2 )
        {
            character = new PlayerCharacter( name );
        }
        m_characters.push_back( character );
    }
}

void BattleProgram::Cleanup()
{
    for ( auto& character : m_characters )
    {
        if ( character != nullptr )
        {
            delete character;
            character = nullptr;
        }
    }
}
