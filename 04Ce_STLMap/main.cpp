#include <iostream>
#include <map>
#include <string>
using namespace std;

// Map documentation:
// http://www.cplusplus.com/reference/map/map/

void AddItem( map<string, string>& myMap );
void GetItem( map<string, string>& myMap );
void Display( map<string, string>& myMap );
void Clear( map<string, string>& myMap );

int main()
{
    map<string, string> myMap;

    bool done = false;
    while ( !done )
    {
        cout << "---------------------------------------" << endl;
        cout << "MAIN MENU" << endl;
        cout << "Map size: " << myMap.size() << endl << endl;

        cout << "1. Add Item" << endl;
        cout << "2. Get Item" << endl;
        cout << "3. Display" << endl;
        cout << "4. Clear" << endl;
        cout << "5. Exit" << endl;

        int choice;
        cout << ">> ";
        cin >> choice;

        if      ( choice == 1 )     { AddItem( myMap ); }
        else if ( choice == 2 )     { GetItem( myMap ); }
        else if ( choice == 3 )     { Display( myMap ); }
        else if ( choice == 4 )     { Clear( myMap ); }
        else if ( choice == 5 )     { done = true; }
    }
}

/**
http://www.cplusplus.com/reference/map/map/operator[]/
mapped_type& operator[] (const key_type& k);

If k matches the key of an element in the container, the function returns a reference to its mapped value.

If k does not match the key of any element in the container, the function inserts a new element with that
key and returns a reference to its mapped value. Notice that this always increases the container size by
one, even if no mapped value is assigned to the element (the element is constructed using its default constructor).
*/
void AddItem( map<string, string>& myMap )
{
}

/**
http://www.cplusplus.com/reference/map/map/find/
iterator find (const key_type& k);
Searches the container for an element with a key equivalent to k and returns an
iterator to it if found, otherwise it returns an iterator to map::end.
*/
void GetItem( map<string, string>& myMap )
{
}

/**
Range-based for loop
*/
void Display( map<string, string>& myMap )
{
}

/**
http://www.cplusplus.com/reference/map/map/clear/
void clear() noexcept;
Removes all elements from the map container (which are destroyed), leaving the container with a size of 0.
*/
void Clear( map<string, string>& myMap )
{
}

