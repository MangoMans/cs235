#include <iostream>
#include <vector>
#include <string>
using namespace std;

// Vector documentation:
// https://www.cplusplus.com/reference/vector/vector/

void PushBack( vector<string>& vec );
void PopBack( vector<string>& vec );
void Clear( vector<string>& vec );
void Display1( vector<string>& vec );
void Display2( vector<string>& vec );

int main()
{
    vector<string> myVec;

    bool done = false;
    while ( !done )
    {
        cout << "---------------------------------------" << endl;
        cout << "MAIN MENU" << endl;
        cout << "Vector size: " << myVec.size() << endl << endl;

        cout << "1. Push Back" << endl;
        cout << "2. Pop Back" << endl;
        cout << "3. Clear" << endl;
        cout << "4. Display 1" << endl;
        cout << "5. Display 2" << endl;
        cout << "6. Exit" << endl;

        int choice;
        cout << ">> ";
        cin >> choice;

        if      ( choice == 1 )     { PushBack( myVec ); }
        else if ( choice == 2 )     { PopBack( myVec ); }
        else if ( choice == 3 )     { Clear( myVec ); }
        else if ( choice == 4 )     { Display1( myVec ); }
        else if ( choice == 5 )     { Display2( myVec ); }
        else if ( choice == 6 )     { done = true; }
    }
}

/**
https://www.cplusplus.com/reference/vector/vector/push_back/
void push_back (const value_type& val);
Adds a new element at the end of the vector, after its current last element. The content of val is copied (or moved) to the new element.
*/
void PushBack( vector<string>& vec )
{
}

/**
https://www.cplusplus.com/reference/vector/vector/pop_back/
void pop_back();
Removes the last element in the vector, effectively reducing the container size by one.
*/
void PopBack( vector<string>& vec )
{
}

/**
https://www.cplusplus.com/reference/vector/vector/clear/
void clear();
Removes all elements from the vector (which are destroyed), leaving the container with a size of 0.
*/
void Clear( vector<string>& vec )
{
}

/**
https://www.cplusplus.com/reference/vector/vector/operator[]/
reference operator[] (size_type n);
Returns a reference to the element at position n in the vector container.
*/
void Display1( vector<string>& vec )
{
}

/**
Range-based for loop
*/
void Display2( vector<string>& vec )
{
}

