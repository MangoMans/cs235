import subprocess
import sys
from subprocess import PIPE, run
import os
from os import listdir
from os.path import isfile, join

def FindCodeblocksFile( currentPath ):
    for root, subdirs, files in os.walk( currentPath ):
        for f in files:
            if ( f.find( ".cbp" ) != -1 ):                
                return root, os.path.join( root, f )
    return ""

def SanitizePath( path ):
    return path.replace( " ", "\ " )
    
# Program begins

# Arguments:  sys.argv
# Expected arguments: python3 grader.py class assignment

print( "" )
print( "ARGUMENTS" )
for i in range( len( sys.argv ) ):
    print( str( i ) + ": " + sys.argv[i], end="\t" )

projectPath = sys.argv[1]

os.chdir( projectPath )
#print( "\n PWD:", os.getcwd() )

cbpPath, cbpFilePath = FindCodeblocksFile( "." )
cbpPath = SanitizePath( cbpPath )
cbpFilePath = SanitizePath( cbpFilePath )

#print( "\n CODEBLOCKS PROJECT PATH:", cbpPath )

os.system( "codeblocks --build " + cbpFilePath )

exePath = os.path.join( cbpPath, "bin" )
exePath = os.path.join( exePath, "Debug" )
exePath = os.path.join( exePath, "TestingLab" )

#print( "\n EXE PATH:", exePath )

outputPath = os.path.join( os.getcwd(), "output.txt" )
outputPath = SanitizePath( outputPath )

#print( "\n OUTPUT PATH:", outputPath )

os.system( "mv test-result.txt output.txt" )
os.system( exePath + " >> " + outputPath )
