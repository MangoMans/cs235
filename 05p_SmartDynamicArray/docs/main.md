# SmartDynamicArray

## View class documentation:

Go to the SmartDynamicArray page to view documentation for the data structure.

----------------------------

## About the SmartDynamicArray:
<img src="../diagrams.png" style="float:right;" title="UML Diagram of SmartDynamicArray">

Our SmartDynamicArray class will be similar to C++'s
[vector](https://www.cplusplus.com/reference/vector/vector/)
object that is part of the Standard Template Library.

The idea behind this structure is that we can insert, remove, and access
data at any position. There are also helper functions that deal with things
such as allocating memory, doing error checks, and moving elements around.

### Design notes:

#### No gaps allowed

The array is designed to not allow any gaps in data. That means 
we can have an array like this:

 0 | 1 | 2 | 3 | 4
---|---|---|---|---
 A | B | C | D | -

But not like this:

 0 | 1 | 2 | 3 | 4
---|---|---|---|---
 A | B | - | C | D
 
Helper functions like SmartDynamicArray::ShiftLeft() and SmartDynamicArray::ShiftRight() are used to make sure
we have no gaps in the array.

<br><br>
#### Array indices

Because we don't allow any gaps, there are some assumptions we can make:

1. The **first element** of the array will always be at index 0.
2. The **last element** of the array will always be at index `m_itemCount-1`.

<div style="clear:both;"></div>

<br><br>
#### throw NotImplementedException

Many of the functions in the SmartDynamicArray contain the following code initially:

```
template <typename T>
void SmartDynamicArray<T>::AllocateMemory( int size )
{
    throw NotImplementedException( "AllocateMemory not implemented yet" ); // Erase this once you work on this function
}
```

This exception is here to tell the unit tests when a function has not yet
been implemented. Once you begin working on a function,
remove this **throw** line of code.


<br><br>


### Function illustrations:

This portion is to show you visually how the array would look during different operations.
Make sure to **also read the documentation** to understand how to implement these things.

#### InsertBack()

Let's say we're starting with an array with these elements:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | D | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 4
 
The function `InsertBack( "Z" )` is then called.
The next available empty space is at index 4, which is also
the value of the variable `m_itemCount`.
We can insert the new item at `m_array[m_itemCount] = newItem`.

The result will be this:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | D | Z | - 
 
**See documentation: SmartDynamicArray::InsertBack()**

<br><br>

#### InsertFront()

Let's say we're starting with an array with these elements:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | D | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 4.

The function `InsertFront( "Z" )` is then called.
It will be inserted at index 0, but we don't want to overwrite A.
So, we must call SmartDynamicArray::ShiftRight() to move A to 1, B to 2, C to 3, and D to 4.
The array will then look like this:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | A | B | C | D | - 
 
A has been copied over, but it doesn't matter what is at position 0 at this point.
After the ShiftRight() we can put our Z value at position 0 with
`m_array[0] = newItem`.
 
 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 Z | A | B | C | D | - 
 
**See documentation: SmartDynamicArray::InsertFront()**

<br><br>
#### InsertAt()

Let's say we're starting with an array with these elements:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | D | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 4

The function `InsertAt( "Z", 2 )` is then called.
We want to insert Z between B and C, so Z will go at index 2.
This means we have to use SmartDynamicArray::ShiftRight() again,
but from position 2 to make room for Z. After ShiftRight(),
it should look like this:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | C | D | - 
 
It doens't matter what is stored at index 2 at the moment, because we will
be overwriting it with Z with `m_array[index] = newItem`.
 
 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | Z | C | D | - 
 
**See documentation: SmartDynamicArray::InsertAt**

<br><br>

#### RemoveBack()

Let's say we're starting with an array with these elements:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | D | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 4

The function `RemoveBack()` is called. We want to remove the last item,
but we don't actually have to do anything with D. Instead, we can simply
decrement `m_itemCount`. D will still be there, but it will be overwritten
next time we do an `InsertBack()`.

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | D | - | - 
valid | valid | valid | to be overwritten | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 3 - valid indices are 0, 1, and 2.
 
**See documentation: SmartDynamicArray::RemoveBack**


<br><br>

#### RemoveFront()
 
Let's say we're starting with an array with these elements:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | D | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 4

The function `RemoveFront()` is called. All we need to do is shift the
elements of the array to the left to overwrite our first item.
We can call SmartDynamicArray::ShiftLeft() to do this. The result is this:
 
 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 B | C | D | D | - | - 
 valid | valid | valid | to be overwritten | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 3

Even if D is still in position 3, it's outside the valid indices now,
and will be overwritten next time `InsertBack()` is called.
 
**See documentation: SmartDynamicArray::RemoveFront**

<br><br>

#### RemoveAt()
 
Let's say we're starting with an array with these elements:

 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | B | C | D | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 4

The function `RemoveAt( 1 )` is called. We again just use SmartDynamicArray::ShiftLeft()
to move everything left by one space to overwrite B. The result is this:
 
 0 | 1 | 2 | 3 | 4 | 5
---|---|---|---|---|---
 A | C | D | D | - | - 
 valid | valid | valid | to be overwritten | - | - 
 
* `m_arraySize` is 6
* `m_itemCount` is 3
 
**See documentation: SmartDynamicArray::RemoveAt**

 
<br><br>

----------------------------

## About the cuTEST unit tests:
<img src="../cutest.png" style="margin: 0 auto; display: block;" title="Screenshot of example test run">

Unit tests are included with this data structure. After you run the program
and run the tests, it will output a file called `test_result.html` in your project directory.
Open this up to view the test results in detail.

The result table will include the following:

Column                      | Description
----------------------------|--------------------------------------------------------------------------------
**Test set**                | What function is being tested?
**Test**                    | One specific test, what is this test trying to do?
**Prerequisite functions**  | This test set may require another function to have already been implemented.
**Pass/fail**               | Did the test pass or fail?
**Expected output**         | What the test was expecting as a result.
**Actual output**           | What was actually discovered as the result.
**Comments**                | Some tests have comments to help you diagnose errors.

The test sets will check for average usage as well as error cases. Make sure to use
the test results to help you validate your program logic.



















