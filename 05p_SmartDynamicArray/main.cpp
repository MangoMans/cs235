#include "DataStructure/SmartDynamicArrayTester.hpp"
#include "ProgramMenu.hpp"
#include "Utilities/Logger.hpp"
#include "Utilities/Menu.hpp"

int main()
{
    Logger::Setup();

    bool done = false;
    while ( !done )
    {
        Menu::Header( "SMART DYNAMIC ARRAY PROJECT" );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Run tests",
            "Run program",
            "Exit"
        } );

        switch( choice )
        {
            case 1:
            {
                SmartDynamicArrayTester tester;
                tester.Start();
            }
            break;

            case 2:
            {
                ProgramMenu menu;
                menu.Run();
            }
            break;

            case 3:
                done = true;
            break;
        }
    }

    Logger::Cleanup();

    return 0;
}
