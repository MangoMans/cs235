#ifndef _TESTS_HPP
#define _TESTS_HPP

#include <fstream>
#include <string>
#include <iostream>
using namespace std;

#include "Functions.hpp"

// -----------------------------------------------
// - Tests that have already been written
// -----------------------------------------------
void Test_GetPerimeter( ofstream& output )
{
    output << endl << "------------------------------------" << endl;
    output << "Test_GetPerimeter:" << endl;

    cout << "Test_GetPerimeter:\t ";
    bool allPass = true;

    int width, length;
    int expectedOutput;
    int actualOutput;

    // Test 1
    width = 7;
    length = 3;
    expectedOutput = 20;
    actualOutput = GetPerimeter( width, length );

    output << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " width: " << width << ", length: " << length << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 2
    width = 8;
    length = 5;
    expectedOutput = 26;
    actualOutput = GetPerimeter( width, length );

    output << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " width: " << width << ", length: " << length << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    if ( allPass )  { cout << "PASS" << endl; }
    else            { cout << "FAIL" << endl; }
}

void Test_GetFirstLetter( ofstream& output )
{
    output << endl << "------------------------------------" << endl;
    output << "Test_GetFirstLetter:" << endl;

    cout << "Test_GetFirstLetter:\t ";
    bool allPass = true;

    string text;
    char expectedOutput;
    char actualOutput;

    // Test 1
    text = "ABC";
    expectedOutput = 'A';
    actualOutput = GetFirstLetter( text );

    output << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " text: " << text << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 2
    text = "xyz";
    expectedOutput = 'x';
    actualOutput = GetFirstLetter( text );

    output << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " text: " << text << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    if ( allPass )  { cout << "PASS" << endl; }
    else            { cout << "FAIL" << endl; }
}

void Test_Withdraw( ofstream& output )
{
    output << endl << "------------------------------------" << endl;
    output << "Test_Withdraw:" << endl;

    cout << "Test_Withdraw:\t\t ";
    bool allPass = true;

    float balance, amount;
    float expectedOutput;
    float actualOutput;

    // Test 1
    balance = 100;
    amount = 100;
    expectedOutput = 0;
    actualOutput = Withdraw( balance, amount );

    output << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " balance: " << balance << ", amount: " << amount << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 2
    balance = 100;
    amount = 500;
    expectedOutput = 100;   // expect no change
    actualOutput = Withdraw( balance, amount );

    output << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " balance: " << balance << ", amount: " << amount << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 3
    balance = 0;
    amount = 10;
    expectedOutput = 0;   // expect no change
    actualOutput = Withdraw( balance, amount );

    output << endl << "Test 3... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " balance: " << balance << ", amount: " << amount << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 4
    balance = 20;
    amount = 10;
    expectedOutput = 10;
    actualOutput = Withdraw( balance, amount );

    output << endl << "Test 4... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " balance: " << balance << ", amount: " << amount << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    if ( allPass )  { cout << "PASS" << endl; }
    else            { cout << "FAIL" << endl; }
}


// -----------------------------------------------
// - Tests that need to be written
// -----------------------------------------------
void Test_GetArea( ofstream& output )
{
    output << endl << "------------------------------------" << endl;
    output << "Test_GetArea:" << endl;

    cout << "Test_GetArea:\t\t ";
    bool allPass = true;

    int width, length;
    int expectedOutput;
    int actualOutput;

    // Test 1
    width = 10;
    length = 10;
    expectedOutput = 100;
    actualOutput = GetArea (width, length);
    output << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " width: " << width << " length: " << length << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 2
    width = 7;
    length = 3;
    expectedOutput = 20;
    actualOutput = GetPerimeter( width, length );

    output << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " width: " << width << ", length: " << length << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }
}

void Test_CombineText( ofstream& output )
{
    output << endl << "------------------------------------" << endl;
    output << "Test_CombineText:" << endl;

    cout << "Test_CombineText:\t ";
    bool allPass = true;

    string str1, str2;
    string expectedOutput;
    string actualOutput;

    // Test 1
    str1 = "nice";
    str2 = " code";
    expectedOutput = "nice code";
    actualOutput = CombineText( str1, str2 );
    output << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " String 1: " << str1 << ", String 2: " << str2 << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 2
    str1 = "It";
    str2 = " Works!";
    expectedOutput = "It Works!";
    actualOutput = CombineText( str1, str2 );
    output << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " String 1: " << str1 << ", String 2: " << str2 << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }
}

void Test_AbsoluteValue( ofstream& output )
{
    output << endl << "------------------------------------" << endl;
    output << "Test_AbsoluteValue:" << endl;

    cout << "Test_AbsoluteValue:\t ";
    bool allPass = true;

    int number;
    int expectedOutput;
    int actualOutput;

    // Test 1
    number = -10;
    expectedOutput = 10;
    actualOutput = AbsoluteValue( number );
    output << endl << "Test 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " number: " << number << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 2
    number = 10;
    expectedOutput = 10;
    actualOutput = AbsoluteValue( number );
    output << endl << "Test 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " number: " << number << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }

    // Test 3
    number = 0;
    expectedOutput = 0;
    actualOutput = AbsoluteValue( number );
    output << endl << "Test 3... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl
            << " number: " << number << endl
            << " expected output: " << expectedOutput << endl
            << " actual output:   " << actualOutput << endl;
        allPass = false;
    }
}



#endif
