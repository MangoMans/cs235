#ifndef _FUNCTIONS_HPP
#define _FUNCTIONS_HPP

#include <string>
using namespace std;

// -----------------------------------------------
// - Functions that need to be fixed
// -----------------------------------------------

/**
    Calculates and returns the perimeter of a rectangle.
*/
int GetPerimeter( int width, int length )
{
    return 2 * width + 2 * length;
}

/**
    Gets the first letter of a string.
    Use the subscript operator with the string
*/
char GetFirstLetter( string text )
{
    return text[0];
}

/**
    Adjusts the balance based on the amount withdrawn
    and returns the new balance.
    If the amount to withdraw is more than the balance,
    no change is made.
*/
float Withdraw( float balance, float amount )
{
    if (balance >= amount || balance == amount )
    {
        return balance - amount;
    }
    else
    {
        return balance;
    }

}

/**
    Calculates and returns the area of a rectangle.
*/
int GetArea( int width, int length )
{
    return width * length;
}

/**
    Combines two strings together.
*/
string CombineText( string str1, string str2 )
{
    return str1 + str2;
}

/**
    Returns the absolute value of the given number.
*/
int AbsoluteValue( int number )
{
    if ( number >= 0 || number == 0)
    {
        return number;
    }
    else
    {
        return number * -1;
    }
}

#endif
