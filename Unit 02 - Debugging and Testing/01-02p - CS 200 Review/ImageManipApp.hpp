#ifndef _IMAGE_MANIP_APP_HPP
#define _IMAGE_MANIP_APP_HPP

#include "Image.hpp"

#include <string>
#include <vector>
using namespace std;

class ImageManipApp
{
    public:
    ImageManipApp();
    ImageManipApp( string imagePath );

    void SetImagePath( string imagePath );

    void Startup();
    void MainMenu();
    void Run();

    void LoadImage();
    void ApplyFilter();
    void SaveImage();

    private:
    string m_imagePath;
    string m_loadedImageName;
    vector<int> m_appliedFilters;
    PpmImage m_loadedImage;
    PpmImage m_filteredImage;

    string ConvertPath( string path, string userInput );
    void DisplayLoadedImage();
    void DisplayAppliedFilters();
    string BuildImageFilename();
};

#endif
