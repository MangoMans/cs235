// 37afb752ea4775edd33e08b3c297751df5496532
#include "Image.hpp"

#include <fstream>
#include <iostream>
using namespace std;

PpmImage::PpmImage()
{
    m_pixels = new Pixel*[IMAGE_WIDTH];

    for ( int i = 0; i < IMAGE_WIDTH; i++ )
    {
        m_pixels[i] = new Pixel[IMAGE_HEIGHT];
    }
}

PpmImage::~PpmImage()
{
    for ( int i = 0; i < IMAGE_WIDTH; i++ )
    {
        delete [] m_pixels[i];
    }

    delete [] m_pixels;
}

/**
@param      filename      image file to load
@return     true when image loaded successfully, false otherwise.

HEADER FORMAT:
P3
# Created by GIMP version 2.10.18 PNM plug-in
300 300
255

AFTER THE HEADER IS A SERIES OF RED, GREEN, BLUE VALUES FOR EACH PIXEL:
51
46
25
(ETC.)
*/
bool PpmImage::LoadImage( const string& filename )
{
    cout << "\n Loading image from \"" << filename << "\"..." << endl;

    ifstream input( filename );

    if ( input.fail() )
    {
        return false;
    }

    string strBuffer;
    int intBuffer;

    getline( input, strBuffer ); // P3
    getline( input, strBuffer ); // Glimpse comment

    input >> intBuffer >> intBuffer; // width and height
    input >> m_colorDepth; // color depth

    // Read pixels
    int x = 0, y = 0;
    while ( input
        >> m_pixels[x][y].r
        >> m_pixels[x][y].g
        >> m_pixels[x][y].b
        )
    {
        x++;
        if ( x >= IMAGE_WIDTH )
        {
            y++;
            x = 0;
        }
        if ( y >= IMAGE_HEIGHT )
        {
            // ignore... don't go out of bounds.
            y = IMAGE_HEIGHT - 1;
        }
    }

    return true;
}

/**
@param      filename      filepath to save image to
@return     true when image saved successfully, false otherwise.

MAKE SURE TO OUTPUT ALL THIS INFORMATION,
WITH PROPER WHITESPACING:

P3
# Created by GIMP version 2.10.18 PNM plug-in
300 300
255
51
46
25

P3      - hard code the file type P3
# etc.  - Put whatever you want for the comment
300 300 - width and height, separated  by a space
255     - color depth
51      - pixel 0 red
46      - pixel 0 green
25      - pixel 0 blue
(etc.)
*/
bool PpmImage::SaveImage( const string& filename )
{
    cout << "\n Saving image to \"" << filename << "\"..." << endl;

    ofstream output( filename );

    if ( output.fail() )
    {
        return false;
    }

    // Output header stuff
    output << "P3" << endl
        << "# Comment blah blah blah" << endl
        << IMAGE_WIDTH << " "
        << IMAGE_HEIGHT << endl
        << m_colorDepth << endl;

    // Output all the pixels
    for ( int y = 0; y < IMAGE_HEIGHT; y++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH; x++ )
        {
            output
                << m_pixels[x][y].r << endl
                << m_pixels[x][y].g << endl
                << m_pixels[x][y].b << endl;
        }
    }

    return true;
}

void PpmImage::operator= ( const PpmImage& other )
{
    m_colorDepth = other.m_colorDepth;
    for ( int y = 0; y < IMAGE_HEIGHT; y++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH; x++ )
        {
            m_pixels[x][y].r = other.m_pixels[x][y].r;
            m_pixels[x][y].g = other.m_pixels[x][y].g;
            m_pixels[x][y].b = other.m_pixels[x][y].b;
        }
    }
}

void PpmImage::ApplyFilter1()
{
    cout << "Applying filter 1..." << endl;

    // Reverse pixels
    for ( int y = 0; y < IMAGE_HEIGHT; y++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH; x++ )
        {
            int y2 = IMAGE_HEIGHT - 1 - y;
            int x2 = IMAGE_WIDTH - 1 - x;

            m_pixels[x][y].r = m_pixels[x2][y2].r;
            m_pixels[x][y].g = m_pixels[x2][y2].g;
            m_pixels[x][y].b = m_pixels[x2][y2].b;
        }
    }
}

void PpmImage::ApplyFilter2()
{
    cout << "Applying filter 2..." << endl;

    // Reverse pixels
    for ( int y = 0; y < IMAGE_HEIGHT; y++ )
    {
        for ( int x = 0; x < IMAGE_WIDTH; x++ )
        {
            int r = m_pixels[x][y].r;
            int g = m_pixels[x][y].g;
            int b = m_pixels[x][y].b;

            m_pixels[x][y].r = g;
            m_pixels[x][y].g = b;
            m_pixels[x][y].b = r;
        }
    }
}

void PpmImage::ApplyFilter3()
{
    cout << "Applying filter 3..." << endl;
}

void PpmImage::ApplyFilter4()
{
    cout << "Applying filter 4..." << endl;
}
