#ifndef _BATTLE_PROGRAM_HPP
#define _BATTLE_PROGRAM_HPP

#include "Character.hpp"

#include <vector>
using namespace std;

class BattleProgram
{
    public:
    BattleProgram();
    ~BattleProgram();

    void Run();

    private:
    bool m_done;
    int m_round;
    vector<Character*> m_characters;

    void Setup();
    void Cleanup();
    void DisplayStats();
    void HorizBar();
};

#endif
