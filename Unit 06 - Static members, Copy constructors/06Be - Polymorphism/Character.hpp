#ifndef _CHARACTER_HPP
#define _CHARACTER_HPP

#include <string>
using namespace std;

class Character
{
    public:
    Character( string name );
    virtual ~Character() {}

    string GetName();
    int GetHP();
    int GetStrength();
    int GetDefense();
    string GetType();
    bool IsDead();
    void DisplayInfo();

    virtual int Action() = 0;
    virtual int AttackWho( int maxIndex ) = 0;

    void Heal( int amount );
    void GetAttacked( int damage );

    protected:
    string m_name;
    int m_hp;
    const int MAX_HP;
    int m_strength;
    int m_defense;
    string m_type;
};

class NonPlayerCharacter : public Character
{
    public:
    NonPlayerCharacter( string name );
    virtual ~NonPlayerCharacter() {}

    virtual int Action();
    virtual int AttackWho( int maxIndex );
};

class PlayerCharacter : public Character
{
    public:
    PlayerCharacter( string name );
    virtual ~PlayerCharacter() {}

    virtual int Action();
    virtual int AttackWho( int maxIndex );
};

#endif
