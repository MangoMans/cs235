#include "BattleProgram.hpp"

int main()
{
    BattleProgram program;
    program.Run();

    return 0;
}
