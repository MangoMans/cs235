#include "StudentProgram.hpp"
#include "StudentManager.hpp"

#include "tester/utilities/Menu.hpp"

void StudentProgram::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Add a student",
            "Remove a student",
            "Find student by ID",
            "List all students",
            "Quit"
        } );

        switch( choice )
        {
            case 1: Menu_AddStudent();      break;
            case 2: Menu_RemoveStudent();   break;
            case 3: Menu_GetStudent();      break;
            case 4: Menu_ListStudents();    break;
            case 5: done = true;            break;
        }
    }
}

void StudentProgram::Menu_AddStudent()
{
    Menu::ClearScreen();
    Menu::Header( "ADD STUDENT" );

    string name = Menu::GetStringLine( "Enter student's full name" );
    int id = Menu::GetIntChoice( "Enter student's ID" );

    StudentManager::AddStudent( name, id );

    cout << "Student added." << endl;

    Menu::Pause();
}

void StudentProgram::Menu_RemoveStudent()
{
    Menu::ClearScreen();
    Menu::Header( "REMOVE STUDENT" );

    StudentManager::ListStudents();

    int id = Menu::GetIntChoice( "Enter student's ID" );

    try
    {
        // StudentManager::RemoveStudent( id );
        cout << "Student removed." << endl;
    }
    catch( runtime_error ex )
    {
        cout << "ERROR: " << ex.what() << endl;
    }

    Menu::Pause();
}

void StudentProgram::Menu_GetStudent()
{
    Menu::ClearScreen();
    Menu::Header( "FIND STUDENT" );

    int id = Menu::GetIntChoice( "Enter student's ID" );

    try
    {
        Student& student = StudentManager::GetStudent( id );
        cout << "Student found: " << student.name << endl;
    }
    catch( runtime_error ex )
    {
        cout << "ERROR: " << ex.what() << endl;
    }

    Menu::Pause();
}

void StudentProgram::Menu_ListStudents()
{
    Menu::ClearScreen();
    Menu::Header( "LIST STUDENTS" );

    cout << "Total students: " << StudentManager::GetStudentCount() << endl << endl;

    StudentManager::ListStudents();

    Menu::Pause();
}

