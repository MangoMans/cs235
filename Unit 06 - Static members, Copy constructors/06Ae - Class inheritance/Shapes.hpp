#ifndef _SHAPES_HPP
#define _SHAPES_HPP

#include <iostream>
#include <string>
using namespace std;

class Shape
{
    public:
    void Setup( string unit );
    void DisplayArea();

    protected:
    float CalculateArea();

    string m_unit;
};

class Rectangle : public Shape
{
    public:
    void Setup( float width, float height, string unit );
    void DisplayArea();

    protected:
    float CalculateArea();

    float m_width, m_height;
};

class Circle : public Shape
{
    public:
    void Setup( float radius, string unit );
    void DisplayArea();

    protected:
    float CalculateArea();

    float m_radius;
};

#endif
