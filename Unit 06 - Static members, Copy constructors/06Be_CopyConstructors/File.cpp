#include "File.hpp"

#include <iostream>
using namespace std;

File::File()
{
    m_contents = "";
    m_bytes = 0;
}


File::File(File& other)
{
    m_extension = other.m_extension;
    m_bytes = other.m_bytes;
    m_contents = other.m_contents;
    m_name = other.m_name + "(Copy)";
}
// Write a copy constructor:
// Copy over the file's contents, bytes, and extension
// But add "(Copy)" to the end of the copied name.

void File::Create( string filename, string extension )
{
    m_name = filename;
    m_extension = extension;
    m_bytes = 0;
}

void File::AddContents( string text )
{
    text += "\n";
    m_contents += text;
    m_bytes += sizeof( text );
}

void File::DisplayInfo()
{
    cout << m_name << "."
        << m_extension << " (" << m_bytes << " B)" << endl;
}

void File::DisplayContents()
{
    cout << m_contents << endl;
}
