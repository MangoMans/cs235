#ifndef _FILE_HPP
#define _FILE_HPP

#include <string>
using namespace std;

class File
{
    public:
    File();

    File( File& other);

    void Create( string filename, string extension );
    void AddContents( string text );
    void DisplayInfo();
    void DisplayContents();

    private:
    string m_name;
    string m_extension;
    string m_contents;
    int m_bytes;
};

#endif
