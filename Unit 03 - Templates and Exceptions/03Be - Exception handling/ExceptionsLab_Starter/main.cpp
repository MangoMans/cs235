#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdexcept>
using namespace std;

float Division( int num, int denom )
{
    if ( denom == 0)
    {
        throw runtime_error( "You can't divide by zero." );
    }
    return float(num) / float(denom);
}

void Test_Division( int min, int max )
{
    ofstream output( "result.txt" );
    for ( int n = min; n <= max; n++ )
    {
        for ( int d = min; d <= max; d++ )
        {
            output << left <<
                setw( 3 ) << n <<
                setw( 3 ) << "/" <<
                setw( 3 ) << d << " = ";

            float result = 0;

            // TODO: Wrap this in a try/catch
            try
            {
                result = Division( n, d );
                output << setw( 20 ) << result << endl;
            }
            catch (runtime_error ex)
            {

                ex.what();
                cout << "Divide by 0!" << endl;
            }

        }
    }
}

int main()
{
    Test_Division(-2, 2 );

    return 0;
}
