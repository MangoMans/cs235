import subprocess
import sys
from subprocess import PIPE, run
import os
from os import listdir
from os.path import isfile, join

def FindCodeblocksFile( currentPath ):
    print( "\n * testscript.py - FindCodeblocksFile" )
    for root, subdirs, files in os.walk( currentPath ):
        for f in files:
            if ( f.find( ".cbp" ) != -1 ):
                if ( ".save" in f ): continue
                return root, os.path.join( root, f )
    return "", ""

def FindSourceFile( currentPath ):
    print( "\n * testscript.py - FindSourceFile" )
    for root, subdirs, files in os.walk( currentPath ):
        for f in files:
            if ( f.find( ".cpp" ) != -1 ):                
                return os.path.join( root, f )
    return ""

def SanitizePath( path ):
    return path.replace( " ", "\ " )
    
# Program begins

# Arguments:  sys.argv
# Expected arguments: python3 grader.py class assignment

print( "" )
for i in range( 80 ): print( "*", end="" )
print( "\n * testscript.py - ARGUMENTS" )
for i in range( len( sys.argv ) ):
    print( "\t" + str( i ) + ": " + sys.argv[i] )

projectPath = os.path.join( os.getcwd(), sys.argv[1] )

print( "\n * testscript.py - projectPath: [[" + projectPath + "]]" )
print( "\n * testscript.py - pwd: [[" + os.getcwd() + "]]" )
os.chdir( projectPath )
print( "\n * testscript.py - pwd: [[" + os.getcwd() + "]]" )


# Check if there's a source file here first
aSourceFile = FindSourceFile( "." )
if ( aSourceFile == "" ):
    print( "\n * testscript.py - NO SOURCE FILES FOUND - MAKE EMPTY OUTPUT" )
    # Not found, just output a empty output
    outputPath = os.path.join( os.getcwd(), "output.txt" )
    outputPath = SanitizePath( outputPath )
    outputFile = open( "output.txt", "w" )
    outputFile.write( "No source file found!" )
    outputFile.close()
    

else:
    # Find the code blocks file
    cbpPath, cbpFilePath = FindCodeblocksFile( "." )
    print( "\n * testscript.py - cbpPath: [[" + cbpPath + "]]" )
    print( "\n * testscript.py - cbpFilePath: [[" + cbpFilePath + "]]" )
    
    if ( cbpFilePath == "" ):
        print( "\n * testscript.py - NO CODEBLOCKS FILE FOUND - USE G++" )
        # No codeblocks file found; use g++
                
        os.chdir( projectPath )
        print( "\n * testscript.py - pwd: [[" + os.getcwd() + "]]" )
        os.system( "ls" )

        source = ( "*.cpp" )
        headers = ( "" )
        command = "g++ " + source + " " + headers + " -o a.out"        
        print( "\n * testscript.py - COMMAND: [[" + command + "]]" )
        os.system( command )
        os.system( "ls" )

        exePath = os.path.join( projectPath, "a.out" )
        print( "\n * testscript.py - exePath: [[" + exePath + "]]" )
        
        
    else:
        cbpPath = SanitizePath( cbpPath )
        cbpFilePath = SanitizePath( cbpFilePath )

        command = "codeblocks --build " + cbpFilePath 
        print( "\n * testscript.py - COMMAND: [[" + command + "]]" )
        os.system( command )

        exePath = os.path.join( cbpPath, "bin" )
        exePath = os.path.join( exePath, "Debug" )
        exePath = os.path.join( exePath, "ExceptionsLab" )       # UPDATE ME
        print( "\n * testscript.py - exePath: [[" + exePath + "]]" )

    outputPath = os.path.join( os.getcwd(), "output.txt" )
    print( "\n * testscript.py - outputPath: [[" + outputPath + "]]" )
    
    print( "\n * testscript.py - RUN PROGRAM AND OUTPUT TO output.txt" )
    
    # cin opreation, num1, num2
    resultPath = os.path.join( os.getcwd(), "result.txt" )
    os.system( "echo ' TEST 1' > " + SanitizePath( outputPath ) )
    command = "echo '2 3 4 1' | " + SanitizePath( exePath ) + " >> " + SanitizePath( outputPath )
    os.system( command )
    
    os.system( "echo '\n FILE OUT' >> " + SanitizePath( outputPath ) )
    
    command = "cat " + SanitizePath( resultPath ) + " >> " + SanitizePath( outputPath )
    os.system( command )


print( "\n* testscript.py - END " )

for i in range( 80 ): print( "*", end="" )
