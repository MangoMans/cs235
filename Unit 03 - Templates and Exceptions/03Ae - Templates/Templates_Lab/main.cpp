#include "Storage/Storage.hpp"
#include "Utilities/Menu.hpp"

#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

void Test_StringStorage();
void Test_IntStorage();
void Test_CharStorage();

int main()
{
    bool done = false;
    while ( !done )
    {
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Exit",
            "String storage",
            "Int storage",
            "Char storage"
            } );

        if      ( choice == 1 )     { done = true; }
        else if ( choice == 2 )     { Test_StringStorage(); }
        else if ( choice == 3 )     { Test_IntStorage(); }
        else if ( choice == 4 )     { Test_CharStorage(); }
    }

    return 0;
}

void Test_StringStorage()
{
    cout << "STORAGE WITH STRINGS" << endl;
    Storage<string> stringStorage;
    stringStorage.Add( "Demon's Souls" );
    stringStorage.Add( "Bugsnax" );
    stringStorage.Add( "Godfall" );
    stringStorage.Add( "Yakuza LaD" );
    stringStorage.Add( "Watchdogs Legion" );
    stringStorage.Add( "Horizon FW" );
    stringStorage.Add( "Final Fantasy 16" );
    stringStorage.Add( "God of War R" );
    stringStorage.Add( "Assassin's Creed V" );
    stringStorage.Display();
}

void Test_IntStorage()
{
    cout << "STORAGE WITH INTEGERS" << endl;

    Storage<int> intStorage;
    for ( int i = 0; i < 25; i++ )
    {
        try
        {
            intStorage.Add( rand() % 100 );
        }
        catch ( runtime_error& ex )
        {
            cout << "Error! " << ex.what() << endl;
        }
    }
    intStorage.Display();
}

void Test_CharStorage()
{
    cout << "STORAGE WITH CHARACTERS" << endl;

    Storage<char> charStorage;
    for ( int i = 0; i < 20; i++ )
    {
        char letter = rand() % 25 + 65;
        charStorage.Add( letter );
    }
    charStorage.Display();
}



