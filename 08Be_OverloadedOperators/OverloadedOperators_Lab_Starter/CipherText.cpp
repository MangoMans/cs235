#include "CipherText.hpp"

CipherText::CipherText()
{
    m_offset = 0;
    m_text = "";
}

CipherText::CipherText( const CipherText& other )
{
    m_offset = other.m_offset;
    m_text = other.m_text;
}

CipherText::CipherText( const CipherText* other )
{
    m_offset = other->m_offset;
    m_text = other->m_text;
}

void CipherText::SetText( string text )
{
    m_text = text;
    m_offset = 0;
}

string CipherText::GetText() const
{
    return m_text;
}

int CipherText::GetOffset() const
{
    return m_offset;
}

void CipherText::Display() const
{
    cout << m_text << endl;
}

void CipherText::OffsetText( int amount )
{
    m_offset += amount;
    for ( unsigned int i = 0; i < m_text.size(); i++ )
    {
        m_text[i] += amount;
    }
}

CipherText CipherText::RemoveOffset()
{
    CipherText temp( this );
    temp = temp - temp.m_offset;
    return temp;
}

CipherText& CipherText::operator=( const CipherText& other )
{
    if ( this == &other ) return *this;

    // Copy stuff over

    return *this;
}

CipherText operator+( const CipherText& item, int amount )
{
    return item; // PLACEHOLDER - REMOVE THIS
}

CipherText operator-( const CipherText& item, int amount )
{
    return item; // PLACEHOLDER - REMOVE THIS
}

bool operator==( const CipherText& a, const CipherText& b )
{
    return false; // PLACEHOLDER - REMOVE THIS
}

ostream& operator<<( ostream& out, CipherText& item )
{
    return out;
}

istream& operator>>( istream& in, CipherText& item )
{
    return in;
}

CipherText CipherText::operator[] ( const int index )
{
    return CipherText(); // PLACEHOLDER - REMOVE THIS
}
