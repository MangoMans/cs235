#ifndef _FUNCTIONS_HPP      // protect against duplicate code error
#define _FUNCTIONS_HPP      // protect against duplicate code error

// FUNCTION DECLARATIONS (Do not modify)

/***********************************************/
/** Function Set 1 ****************************/

string Alphabet_Iter( char start, char end );
string Alphabet_Rec( char start, char end );

/***********************************************/
/** Function Set 2 ****************************/

int Factorial_Iter( int n );
int Factorial_Rec( int n );

/***********************************************/
/** Program and Test runners ******************/

void Program();
void RunTests();

/***********************************************/
/** Utilities *********************************/

string B2S( bool val );
void ClearScreen();
void Pause();

/***********************************************/
/** Test functions ****************************/

void Test_Set1();
void Test_Set2();
void Test_Set3();
void Test_Set4();

const int headerWidth = 70;
const int pfWidth = 10;

#endif                      // protect against duplicate code error
