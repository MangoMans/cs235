#include <iostream>     // Use input and output streams
using namespace std;    // Use the STandarD library

#include "functions.hpp"

int main()
{
    // PROGRAM STARTS HERE

    bool done = false;
    while ( !done )
    {
        cout << endl;
        cout << "******************************" << endl;
        cout << "* Recursion Exercise         *" << endl;
        cout << "******************************" << endl;
    cout << "\a";
        cout << "1. Run tests" << endl;
        cout << "2. Run program" << endl;
        cout << "0. Quit" << endl;

        int choice;
        cout << endl << "SELECTION: ";
        cin >> choice;

        if ( choice == 1 )
        {
            RunTests();
        }
        else if ( choice == 2 )
        {
            Program();
        }
        else if ( choice == 0 )
        {
            done = true;
        }
    }

    // Don't automatically quit (Visual Studio)
    cout << endl << "Press enter to continue" << endl;
    cin.ignore();
    cin.get();

    // PROGRAM ENDS HERE
    return 0;
}
