#include <iostream>         // Use input and output streams
#include <string>           // Using string (for text)
#include <iomanip>          // Special library for formatting text (used in my tests)
using namespace std;        // Use the STandarD library

#include "functions.hpp"    // Function headers are stored here

// USER-DEFINED FUNCTIONS - You will edit these!

/***********************************************/
/** Function Set 1 ****************************/

/**
@param      char        start       The starting char (inclusive) to begin at
@param      char        end         The end char (inclusive) to run until
@return     string                  A string containing all the letters from start to end.
*/
//! Build a string that contains letters from start to end.
string Alphabet_Iter( char start, char end )
{
    string build = "";
    for (char i = start; i <= end; i++)
    {
        build += i;
    }

    return build;
}

/**
@param      char        start       The starting char (inclusive) to begin at
@param      char        end         The end char (inclusive) to run until
@return     string                  A string containing all the letters from start to end.
*/
//! Build a string that contains letters from start to end.
string Alphabet_Rec( char start, char end )
{
    // Terminating Case:
    // Out of letters to go over (in other words, start > end).
    if ( start > end ) { return ""; }

    // Recursive case:
    // Add the next letter, then call return and recurse into this function using the next letter.

    return start + Alphabet_Rec( start+1, end ); // Temporary
}

/***********************************************/
/** Function Set 2 ****************************/

/**
Factorial functions
@param      int     n       The value of n
@return     int             The value of n!

Calculate n! by multiplying n * (n-1) * (n-2) * ... * 3 * 2 * 1.
*/
//! Calculates n!
int Factorial_Iter( int n )
{
    int product = 1;

    for ( int i = 1; i <= n; i++ )
    {
        product = product * i;
    }

    return product;
}

/**
Factorial functions
@param      int     n       The value of n
@return     int             The value of n!

Calculate n! by multiplying n * (n-1) * (n-2) * ... * 3 * 2 * 1.
*/
//! Calculates n!
int Factorial_Rec( int n )
{

    if ( n == 0 || n == 1 ) { return 1; }

    return n * Factorial_Rec( n - 1 );
}

/***********************************************/
/** Function Set 3 ****************************/

//! Helper function to find whether something is a consonant or not.
bool IsConsonant( char letter )
{
    if (    tolower( letter ) == 'a' ||
            tolower( letter ) == 'e' ||
            tolower( letter ) == 'i' ||
            tolower( letter ) == 'o' ||
            tolower( letter ) == 'u'
        )
    {
        return false;
    }

    return true;
}

/**
CountConsonants functions
@param  string  text        The text to count the consonants within
@return int                 The amount of consonants found

Iterate through each char in the string [text] and count up 1 if that letter is a consonant.
Return the amount of consonants found.
*/
//! Count the amount of consonants in a string and return the count.
int CountConsonants_Iter( string text )
{
    return -1; // Temporary
}

/**
CountConsonants functions
@param  string  text        The text to count the consonants of
@param  int     pos         The current position being investigated
@return int                 The amount of consonants found

Recurse through each char in the string [text] and count up 1 if that letter is a consonant.
Return the amount of consonants found.
*/
//! Count the amount of consonants in a string and return the count.
int CountConsonants_Rec( string text, unsigned int pos /* = 0 */ )
{
    // Terminating case:
    // No more letters to look at.

    // Recursive case:
    // Still more letters to inspect.

    return -1; // Temporary
}

/***********************************************/
/** Function Set 4 ****************************/

//! Helper function to figure out if letter is upper-case
bool IsUppercase( char letter )
{
    return ( letter != ' ' && toupper( letter ) == letter );
}

/**
@param  string  text    The text to look for capital letters in
@return char            The first upper-case character found, or ' ' if none found.

Iterate through each char in the string [text] and return the char if it is an upper-case letter.
If no upper-case letters are found, return a space character: ' '
*/
//! Returns the first uppercase letter found, or ' ' if none are found.
char GetFirstUppercase_Iter( string text )
{
    return 'x'; // Temporary
}

/**
@param  string  text    The text to look for capital letters in
@param  int     pos     The current position being investigated
@return char            The first upper-case character found, or ' ' if none found.

Recurse through each char in the string [text] and return the char if it is an upper-case letter.
If no upper-case letters are found, return a space character: ' '
*/
//! Returns the first uppercase letter found, or ' ' if none are found.
char GetFirstUppercase_Rec( string text, unsigned int pos /* = 0 */ )
{
    // Terminating case:
    // No more letters to look at, OR
    // First uppercase letter found.

    // Recursive case:
    // Still more letters to investigate

    return 'x'; // Temporary
}


/** Program code **/
void Program()
{
    bool quit = false;
    while ( quit == false )
    {
//        ClearScreen();
        cout << "***************************************" << endl;
        cout << "**             RECURSION             **" << endl;
        cout << "***************************************" << endl;
        cout << " 1. Alphabet" << endl;
        cout << " 2. Factorial" << endl;
        cout << " 0. Quit" << endl << endl;

        int choice;
        cout << "Run which function? ";
        cin >> choice;

        cout << endl << endl;

        switch( choice )
        {
            case 1:
            {
                char start;
                char end;

                cout << "Enter starting letter and ending letter: ";
                cin >> start >> end;
                cout << endl;

                cout << "Alphabet, Iterative:" << endl;
                string result = Alphabet_Iter( start, end );
                cout << result << endl;

                cout << endl << endl;
                cout << "Alphabet, Recursive:" << endl;
                result = Alphabet_Rec( start, end );
                cout << result << endl;
            }
            break;

            case 2:
            {
                int n;
                cout << "Enter a value for n: ";
                cin >> n;
                cout << endl;

                cout << "Factorial, Iterative:" << endl;
                cout << n << "! = " << Factorial_Iter( n ) << endl;

                cout << endl << endl;
                cout << "Factorial, Recursive:" << endl;
                cout << n << "! = " << Factorial_Rec( n ) << endl;
            }
            break;

            case 0:
                quit = true;
            break;
        }

        cout << endl << endl;
    }
}

// Tester functions (DO NOT MODIFY) ----------------------------------------------------
void RunTests()
{
    Test_Set1();
    Test_Set2();
    Test_Set3();
    Test_Set4();
}

string B2S( bool val )
{
    return ( val ) ? "true" : "false";
}

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    system( "cls" );
    #else
    system( "clear" );
    #endif
}

void Pause()
{
    cout << "Press enter to continue..." << endl;
    cin.ignore();
    cin.get();
}

void Test_Set1()
{
    //string Alphabet_Iter( char start, char end )
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - Alphabet" << endl;
    string expectedOut, actualOut;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Alphabet_Iter: Generate 'a' thru 'g'" << setw( pfWidth );
    expectedOut = "abcdefg";
    actualOut = Alphabet_Iter( 'a', 'g' );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 2: Alphabet_Iter: Generate 'l' thru 'p'" << setw( pfWidth );
    expectedOut = "lmnop";
    actualOut = Alphabet_Iter( 'l', 'p' );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 3: Alphabet_Rec: Generate 'a' thru 'g'" << setw( pfWidth );
    expectedOut = "abcdefg";
    actualOut = Alphabet_Rec( 'a', 'g' );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 4: Alphabet_Rec: Generate 'l' thru 'p'" << setw( pfWidth );
    expectedOut = "lmnop";
    actualOut = Alphabet_Rec( 'l', 'p' );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

}

void Test_Set2()
{
    // int Factorial_Iter( int n );
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - Factorial" << endl;
    int expectedOut, actualOut;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Factorial_Iter: Find 0!" << setw( pfWidth );
    expectedOut = 1;
    actualOut = Factorial_Iter( 0 );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 2: Factorial_Iter: Find 5!" << setw( pfWidth );
    expectedOut = 120;
    actualOut = Factorial_Iter( 5 );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 3: Factorial_Rec: Find 0!" << setw( pfWidth );
    expectedOut = 1;
    actualOut = Factorial_Rec( 0 );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 4: Factorial_Rec: Find 5!" << setw( pfWidth );
    expectedOut = 120;
    actualOut = Factorial_Rec( 5 );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
}

void Test_Set3()
{
}

void Test_Set4()
{
}


