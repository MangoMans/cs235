#include <iostream>
using namespace std;

int main()
{
    int str;
    int dex;
    int con;
    int intt;
    int wis;
    int cha;

    int str_modifier;
    int dex_modifier;
    int con_modifier;
    int intt_modifier;
    int wis_modifier;
    int cha_modifier;

    bool clean = true;

    if ( clean = true )
    {
        cout << "Tell me your stats!" << endl;
        cout << endl;

        cout << "Strength: ";
        cin >> str;
        cout << endl;

        cout << "Dexterity: ";
        cin >> dex;
        cout << endl;

        cout << "Constitution: ";
        cin >> con;
        cout << endl;

        cout << "Intelligence: ";
        cin >> intt;
        cout << endl;

        cout << "Wisdom: ";
        cin >> wis;
        cout << endl;

        cout << "Charisma: ";
        cin >> cha;
        cout << endl;

        // Strength Modifier.
        if ( str <= 9 || str == 8 )
        {
            str = 8;
            str_modifier = 1;
        }

        else if ( str == 10 || str == 11  )
        {
            str_modifier = 0;
        }

        else if ( str == 12 || str == 13  )
        {
            str_modifier = 1;
        }

        else if ( str == 14 || str == 15  )
        {
            str_modifier = 2;
        }

        else if ( str == 16 || str == 17  )
        {
            str_modifier = 3;
        }

        else if ( str == 18 || str == 19  )
        {
            str_modifier = 4;
        }

        else if ( str == 20 )
        {
            str_modifier = 5;
        }

        // Dexterity Modifier.
        if ( dex <= 9 || dex == 8 )
        {
            dex = 8;
            dex_modifier = 1;
        }

        else if ( dex == 10 || dex == 11  )
        {
            dex_modifier = 0;
        }

        else if ( dex == 12 || dex == 13  )
        {
            dex_modifier = 1;
        }

        else if ( dex == 14 || dex == 15  )
        {
            dex_modifier = 2;
        }

        else if ( dex == 16 || dex == 17  )
        {
            dex_modifier = 3;
        }

        else if ( dex == 18 || dex == 19  )
        {
            dex_modifier = 4;
        }

        else if ( dex == 20 )
        {
            dex_modifier = 5;
        }

        //Constitution Modifier.
        if ( con <= 9 || con == 8 )
        {
            con = 8;
            con_modifier = 1;
        }

        else if ( con == 10 || con == 11  )
        {
            con_modifier = 0;
        }

        else if ( con == 12 || con == 13  )
        {
            con_modifier = 1;
        }

        else if ( con == 14 || con == 15  )
        {
            con_modifier = 2;
        }

        else if ( con == 16 || con == 17  )
        {
            con_modifier = 3;
        }

        else if ( con == 18 || con == 19  )
        {
            con_modifier = 4;
        }

        else if ( con == 20 )
        {
            con_modifier = 5;
        }

        //Intelligence Modifiers.
        if ( intt <= 9 || intt == 8 )
        {
            intt = 8;
            intt_modifier = 1;
        }

        else if ( intt == 10 || intt == 11  )
        {
            intt_modifier = 0;
        }

        else if ( intt == 12 || intt == 13  )
        {
            intt_modifier = 1;
        }

        else if ( intt == 14 || intt == 15  )
        {
            intt_modifier = 2;
        }

        else if ( intt == 16 || intt == 17  )
        {
            intt_modifier = 3;
        }

        else if ( intt == 18 || intt == 19  )
        {
            intt_modifier = 4;
        }

        else if ( intt == 20 )
        {
            intt_modifier = 5;
        }

        //Wisdom Modifier.
        if ( wis <= 9 || wis == 8 )
        {
            wis = 8;
            wis_modifier = 1;
        }

        else if ( wis == 10 || wis == 11  )
        {
            wis_modifier = 0;
        }

        else if ( wis == 12 || wis == 13  )
        {
            wis_modifier = 1;
        }

        else if ( wis == 14 || wis == 15  )
        {
            wis_modifier = 2;
        }

        else if ( wis == 16 || wis == 17  )
        {
            wis_modifier = 3;
        }

        else if ( wis == 18 || wis == 19  )
        {
            wis_modifier = 4;
        }

        else if ( wis == 20 )
        {
            wis_modifier = 5;
        }

        //Charisma Modifier.
        if ( cha <= 9 || cha == 8 )
        {
            cha = 8;
            cha_modifier = 1;
        }

        else if ( cha == 10 || cha == 11  )
        {
            cha_modifier = 0;
        }

        else if ( cha == 12 || cha == 13  )
        {
            cha_modifier = 1;
        }

        else if ( cha == 14 || cha == 15  )
        {
            cha_modifier = 2;
        }

        else if ( cha == 16 || cha == 17  )
        {
            cha_modifier = 3;
        }

        else if ( cha == 18 || cha == 19  )
        {
            cha_modifier = 4;
        }

        else if ( cha == 20 )
        {
            cha_modifier = 5;
        }


        cout << "Here are your starts!" << endl;
        cout << endl;

        cout << "Strength: " << str << " +" << str_modifier << endl;
        cout << endl;

        cout << "Dexterity: " << dex << " +" << dex_modifier << endl;
        cout << endl;

        cout << "Constitution: " << con << " +" << con_modifier << endl;
        cout << endl;

        cout << "Intelligence: " << intt << " +" << intt_modifier << endl;
        cout << endl;

        cout << "Wisdom: " << wis << " +" << wis_modifier << endl;
        cout << endl;

        cout << "Charisma: " << cha << " +" << cha_modifier << endl;
        cout << endl;

    }



}





