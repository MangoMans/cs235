#include "CipherText.hpp"

CipherText::CipherText()
{
    m_offset = 0;
    m_text = "";
}

CipherText::CipherText( const CipherText& other )
{
    m_offset = other.m_offset;
    m_text = other.m_text;
}

CipherText::CipherText( const CipherText* other )
{
    m_offset = other->m_offset;
    m_text = other->m_text;
}

void CipherText::SetText( string text )
{
    m_text = text;
    m_offset = 0;
}

string CipherText::GetText() const
{
    return m_text;
}

int CipherText::GetOffset() const
{
    return m_offset;
}

void CipherText::Display() const
{
    cout << m_text << endl;
}

void CipherText::OffsetText( int amount )
{
    m_offset += amount;
    for ( unsigned int i = 0; i < m_text.size(); i++ )
    {
        m_text[i] += amount;
    }
}

CipherText CipherText::RemoveOffset()
{
    CipherText temp( this );
    temp = temp - temp.m_offset;
    return temp;
}

CipherText& CipherText::operator=( const CipherText& other )
{
    if ( this == &other ) return *this;

    // Copy stuff over
    m_offset = other.m_offset;
    m_text = other.m_text;

    return *this;
}

CipherText operator+( const CipherText& item, int amount )
{
    CipherText changed = item;

    changed.OffsetText( amount );

    return changed;
}

CipherText operator-( const CipherText& item, int amount )
{
    CipherText changed = item;

    changed.OffsetText( -amount );

    return changed;
}

bool operator==( const CipherText& a, const CipherText& b )
{
    CipherText unencryptA = a;
    CipherText unencryptB = b;

    unencryptA = unencryptA.RemoveOffset();
    unencryptB = unencryptB.RemoveOffset();

    if ( unencryptA.m_text == unencryptB.m_text )
    {
        return true;
    }
    else
    {
        return false;
    }
}

ostream& operator<<( ostream& out, CipherText& item )
{
    out << item.m_text;
    return out;
}

istream& operator>>( istream& in, CipherText& item )
{
    in >> item.m_text;
    item.m_offset = 0;
    return in;
}

CipherText CipherText::operator[] ( const int index )
{
    CipherText temp( this );
    temp.RemoveOffset();
    temp.OffsetText( index );
    return temp;
}
