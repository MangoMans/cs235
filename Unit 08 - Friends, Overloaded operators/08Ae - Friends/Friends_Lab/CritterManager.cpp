#include "CritterManager.hpp"

#include <cmath>        // sqrt
#include <cstdlib>      // random number generator
#include <ctime>        // time()

#include "utilities/Menu.hpp"
#include "utilities/System.hpp"
#include "utilities/StringUtil.hpp"

CritterManager::CritterManager()
    : REFRESH_RATE ( 500 )
{
    m_mapWidth = 30;
    m_mapHeight = 10;
    m_timeStamp = 0;
    srand( time( NULL ) );
}

void CritterManager::Run()
{
    Setup();

    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "Time: " + StringUtil::ToString( m_timeStamp++ ) );
        Update();
        Draw();
        Delay();
    }

    if ( done )
    {
        Menu::ClearScreen();
        cout << "All the worms got chomped. R.I.P." << endl;
    }
}

void CritterManager::Update()
{
    for ( auto& critter : m_critters )
    {
        critter.Update();

        // Check if the bird caught anything
        if ( critter.m_name == "Bird" )
        {
            for ( auto& otherCritter : m_critters )
            {
                // Has it caught something?
                if ( critter.m_x == otherCritter.m_x
                    && critter.m_y == otherCritter.m_y )
                {
                    if ( StringUtil::Contains( otherCritter.m_name, "Worm" ) )
                    {
                        // Caught a worm
                        otherCritter.m_alive = false;
                        critter.CaughtWorm();
                    }
                }
            }

            // Find a goal to go after
            if ( !critter.HasGoal() )
            {
                int closestWorm = GetClosestWormIndex();
                if ( closestWorm == -1 )
                {
                    critter.SetGoal( GetRandomX(), GetRandomY() );
                }
                else
                {
                    critter.SetGoal( m_critters[closestWorm].m_x, m_critters[closestWorm].m_y );
                }
            }
        }

        // Give the critter a random goal if it's not a bird
        else if ( !critter.HasGoal() )
        {
            int chance = rand() % 5;
            if ( chance == 0 )
            {
                critter.SetGoal( GetRandomX(), GetRandomY() );
            }
            // Otherwise it will sit still
        }
    }
}

int CritterManager::GetClosestWormIndex()
{
    int birdIndex = 0;

    float lowestDistance = -1;
    int lowestIndex = -1;

    for ( unsigned int i = 2; i < m_critters.size(); i++ )
    {
        if ( StringUtil::Contains( m_critters[i].m_name, "Worm" ) )
        {
            float distance = GetDistance(
                m_critters[birdIndex].m_x,  m_critters[birdIndex].m_y,
                m_critters[i].m_x,          m_critters[i].m_y );

            if ( m_critters[i].m_alive )
            {
                if ( lowestDistance == -1 || distance < lowestDistance )
                {
                    lowestDistance = distance;
                    lowestIndex = i;
                }
            }
        }
    }

    return lowestIndex;
}

float CritterManager::GetDistance( float x1, float y1, float x2, float y2 )
{
    // Distance formula
    return sqrt(
        pow( x2 - x1, 2 ) + pow( y2 - y1, 2 )
                );
}

void CritterManager::Setup()
{
    Critter wipCritter;
    wipCritter.m_name = "Bird";
    wipCritter.m_symbol = '%';
    wipCritter.m_x = GetRandomX();
    wipCritter.m_y = GetRandomY();
    m_critters.push_back( wipCritter );

    // Reuse the same variable name but setting
    // up a different critter.
    for ( int i = 0; i < 10; i++ )
    {
        wipCritter.m_name = "Worm" + StringUtil::ToString( i+1 );
        wipCritter.m_symbol = '~';
        wipCritter.m_x = GetRandomX();
        wipCritter.m_y = GetRandomY();
        m_critters.push_back( wipCritter );
    }

    //Another critter bug thing
    for ( int i = 0; i < 5; i++ )
    {
        wipCritter.m_name = "Spider" + StringUtil::ToString( i+1 );
        wipCritter.m_symbol = '*';
        wipCritter.m_x = GetRandomX() +2;
        wipCritter.m_y = GetRandomY() +2;
        m_critters.push_back( wipCritter );
    }
}

void CritterManager::Draw()
{
    for ( int y = 0; y < m_mapHeight; y++ )
    {
        for ( int x = 0; x < m_mapWidth; x++ )
        {
            bool somethingThere = false;
            for ( auto& critter : m_critters )
            {
                if ( critter.m_x == x && critter.m_y == y )
                {
                    somethingThere = true;
                    critter.Draw();
                }
            }

            if ( !somethingThere )
            {
                cout << " ";
            }
        }
        cout << endl;
    }

    cout << "LOG" << endl;
    for ( auto& critter : m_critters )
    {
        critter.OutputLog();
    }
}

void CritterManager::Delay()
{
    System::Sleep(REFRESH_RATE);
}

int CritterManager::GetRandomX()
{
    return rand() % m_mapWidth;
}

int CritterManager::GetRandomY()
{
    return rand() % m_mapHeight;
}
