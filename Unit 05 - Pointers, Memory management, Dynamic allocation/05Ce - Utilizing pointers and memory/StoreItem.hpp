#ifndef _STORE_ITEM_HPP
#define _STORE_ITEM_HPP

#include <string>
using namespace std;

struct StoreItem
{
    StoreItem();

    string name;
    int quantity;
    float price;
};

StoreItem::StoreItem()
{
    name = "UNSET";
    quantity = 0;
    price = 0;
}

#endif
