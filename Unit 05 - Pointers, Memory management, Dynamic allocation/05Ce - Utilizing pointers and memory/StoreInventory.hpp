#ifndef _STORE_INVENTORY_HPP
#define _STORE_INVENTORY_HPP

#include "StoreItem.hpp"

#include "Utilities/Menu.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

class StoreInventory
{
    public:
    StoreInventory();
    ~StoreInventory();

    void Setup( int size );
    void Display() const;
    void SelectActiveItem();
    void EditActiveItem();

    private:
    StoreItem*  m_itemArr;
    int         m_arrSize;
    StoreItem*  m_activeItem;
};

/**
Initializes the class' pointers (m_itemArr and m_activeItem) to nullptr.
Initializes the array size (m_arrSize) to 0.
*/
StoreInventory::StoreInventory()
{
    m_itemArr = nullptr;
    m_activeItem = nullptr;
}

/**
Frees the memory pointed to by m_itemArr if it is pointing to a memory address (not nullptr).
*/
StoreInventory::~StoreInventory()
{
    if (m_itemArr = nullptr )
    {
        delete [] m_itemArr;
    }
}

/**
Allocates space for an array of StoreItem objects of the size passed in.
However, it should check to see if m_itemArr is pointing to nullptr first - if it is NOT
pointing to nullptr, then throw an exception.
*/
void StoreInventory::Setup( int size )
{
    if (m_itemArr != nullptr)
    {
        throw runtime_error(nullptr);
    }

    if (m_itemArr == nullptr)
    {
        m_arrSize = size;
    }
    m_itemArr = new StoreItem[size];
}

/**
Allows the user to select the index of the item they want to
mark as active. If the index is valid (do an error check),
then set the m_activeItem pointer to point to the address
of the element of m_itemArr at the position specified by the user.
*/
void StoreInventory::SelectActiveItem()
{
    int index;

    cout << "Enter an index to set as the active item: ";
    cin >> index;
    if ( index <= 0 || index >= m_arrSize )
    {
        cout << "Nope- try again. Can't be less than 0 or more than the array size." << endl;
        cout << "Enter an index to set as the active item: ";
        cin >> index;
    }
    m_activeItem = &m_itemArr[ index ];
}

/**
Allows the user to edit an inventory item via the m_activeItem pointer.
If m_activeItem is pointing to nullptr, throw an exception. Otherwise,
allow the user to set a new name, quantity and price for the item.
*/
void StoreInventory::EditActiveItem()
{
    if ( m_activeItem = nullptr)
    {
        throw runtime_error(nullptr);
    }
    else
    {
        cout << "Enter a new name, quantity and price: ";
        cin >> m_activeItem->price;
    }
}

/**
Displays all the items in the store inventory.
(Already implemented)
*/
void StoreInventory::Display() const
{
    if ( m_itemArr == nullptr )
    {
        cout << "Inventory not yet set up." << endl;
    }
    else
    {
        cout << left
            << setw( 3 ) << "#"
            << setw( 35 ) << "NAME"
            << setw( 10 ) << "QUANTITY"
            << setw( 10 ) << "PRICE"
            << setw( 10 ) << "ADDRESS"
            << endl;
        Menu::DrawHorizontalBar( 80, '-' );
        for ( int i = 0; i < m_arrSize; i++ )
        {
            cout << left
                << setw( 3 ) << i
                << setw( 35 ) << m_itemArr[i].name
                << setw( 10 ) << m_itemArr[i].quantity
                << setw( 10 ) << m_itemArr[i].price
                << setw( 10 ) << &m_itemArr[i]
                << endl;
        }
    }
    cout << endl;

    cout << "Active item is currently " << m_activeItem << endl;

    cout << endl;
}

#endif
