#include "StoreInventory.hpp"
#include "Utilities/Menu.hpp"

int main()
{
    StoreInventory inv;

    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );
        inv.Display();

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Setup inventory",
            "Select active item",
            "Edit active item",
            "Quit"
        } );

        if ( choice == 1 )
        {
            cout << "How many items in the store? ";
            int size;
            cin >> size;

            // TODO: Call the Setup function, passing in the size. Wrap this in a try/catch.
            try
            {
                inv.Setup( size );
            }
            catch (runtime_error ex)
            {
                cout << (ex.what());
            }

        }
        else if ( choice == 2 )
        {
            inv.SelectActiveItem();
        }
        else if ( choice == 3 )
        {
            // TODO: Call the EditActiveItem() function of inv. Wrap this in a try/catch.
            try
            {
                inv.EditActiveItem();
            }
            catch (runtime_error ex)
            {
                cout << (ex.what());
            }
        }
        else
        {
            done = true;
        }
    }

    return 0;
}
