#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Menu.hpp"

void Program1();
void Program2();
void Program3();

int main()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "Pointers Lab" );
        int choice = Menu::ShowIntMenuWithPrompt(
        {
            "Program 1: Dereferencing pointers",
            "Program 2: Assigning pointers",
            "Program 3: Pointers and Objects",
            "Quit"
        } );

        switch( choice )
        {
            case 1: Program1(); break;
            case 2: Program2(); break;
            case 3: Program3(); break;
            default: done = true;
        }
    }

    return 0;
}

void Program1()
{
    Menu::Header( "Program 1: Dereferencing pointers" );

    bool done = false;
    const int COL_WIDTH = 10;

    int myInteger = 100;
    float myFloat = 9.99;
    string myString = "Hello";

    // Declaring pointers
    int * intPtr;
    float * floatPtr;
    string * stringPtr;

    intPtr = &myInteger;
    floatPtr = &myFloat;
    stringPtr = &myString;

    while ( !done )
    {
        cout << left
            << setw( COL_WIDTH ) << "myInteger"
            << setw( COL_WIDTH ) << "myFloat"
            << setw( COL_WIDTH ) << "myString" << endl;
        Menu::DrawHorizontalBar( 80 );
        cout << setw( COL_WIDTH ) << myInteger
            << setw( COL_WIDTH ) << myFloat
            << setw( COL_WIDTH ) << myString << endl << endl;

        int choice = Menu::ShowIntMenuWithPrompt( { "Update integer", "Update float", "Update string", "Quit" } );
        switch( choice )
        {
            case 1:
                cout << "Current address: " << intPtr << endl;
                cout << "Current value: " << *intPtr << endl;
                cin >> *intPtr;

            break;

            case 2:
                cout << "Current address: " << floatPtr << endl;
                cout << "Current value: " << *floatPtr << endl;
                cin >> *floatPtr;

            break;

            case 3:
                cout << "Current address: " << stringPtr << endl;
                cout << "Current value: " << *stringPtr << endl;
                cin >> *stringPtr;

            break;

            default:
            done = true;
        }
        cout << endl << endl;
    }
}

void Program2()
{
    Menu::Header( "Program 2: Assigning pointers" );

    const int COL_WIDTH = 20;

    bool done = false;

    float priceA = 9.99;
    float priceB = 3.29;
    float priceC = 4.30;

    // Initialize the pointer to nullptr
    float * ptrPrice = nullptr;

    while ( !done )
    {
        cout << left
             << setw( COL_WIDTH ) << "Variable" << setw( COL_WIDTH ) << "priceA" << setw( COL_WIDTH ) << "priceB" << setw( COL_WIDTH ) << "priceC" << endl;
        Menu::DrawHorizontalBar( 80 );
        cout << setw( COL_WIDTH ) << "Value" << setw( COL_WIDTH ) << priceA << setw( COL_WIDTH ) << priceB << setw( COL_WIDTH ) << priceC << endl
             << setw( COL_WIDTH ) << "Address" << setw( COL_WIDTH ) << &priceA << setw( COL_WIDTH ) << &priceB << setw( COL_WIDTH ) << &priceC << endl << endl
             << "Pointer pointing to: " << ptrPrice << endl << endl;

        int choice = Menu::ShowIntMenuWithPrompt( { "Update pointer address", "Update value via pointer", "Quit" } );

        switch( choice )
        {
            case 1:
                cout << "Point to which?" << endl;
                choice = Menu::ShowIntMenuWithPrompt( { "priceA", "priceB", "priceC", "nullptr" } );

                if      ( choice == 1 )
                {
                    ptrPrice = &priceA;
                    // Point to priceA's address
                }
                else if ( choice == 2 )
                {
                    ptrPrice = &priceB;
                    // Point to priceB's address
                }
                else if ( choice == 3 )
                {
                    ptrPrice = &priceC;
                    // Point to priceC's address
                }
                else
                {
                    ptrPrice = nullptr;
                    // Point to nullptr
                }
            break;

            case 2:
                // Error check
                if ( ptrPrice == nullptr )
                {
                    cout << "Cannot update! Pointer is pointing to nullptr!" << endl;
                }
                else
                {
                    float *ptrPrice;
                }
            break;

            default:
            done = true;
        }
        cout << endl << endl;
    }
}



struct Student
{
    string name;
    string school;
};

void Program3()
{
    Menu::Header( "Program 3: Pointers and Objects" );

    const int STUDENT_COUNT = 5;

    bool done = false;

    Student students[STUDENT_COUNT];
    students[0].name = "Seung Lee";             students[0].school = "JCCC";
    students[1].name = "Malik Hussain";         students[1].school = "MCCKC";
    students[2].name = "Remy Marchand";         students[2].school = "KU";
    students[3].name = "Hjalmar Victorsson";    students[3].school = "UMKC";
    students[4].name = "Caiyun Le";             students[4].school = "MS&T";

    Student * ptrStudent = nullptr;
    int index;

    while ( !done )
    {
        cout << left << setw( 10 ) << "Index" << setw( 25 ) << "Name" << setw( 15 ) << "School" << setw( 15 ) << "Address" << endl;
        Menu::DrawHorizontalBar( 80 );
        for ( int i = 0; i < STUDENT_COUNT; i++ )
        {
            cout << left << setw( 10 ) << i << setw( 25 ) << students[i].name << setw( 15 ) << students[i].school << setw( 15 ) << &students[i] << endl;
        }
        cout << endl << endl << "Pointer pointing to: " << ptrStudent << endl << endl;

        int choice = Menu::ShowIntMenuWithPrompt( { "Update pointer address", "Update value via pointer", "Quit" } );

        switch( choice )
        {
            case 1:
                cout << "Point to which? (Give the index #): ";
                index = Menu::GetValidChoice( 0, STUDENT_COUNT - 1 );
                ptrStudent = &students[index];
            break;

            case 2:
                // Error check
                if ( ptrStudent == nullptr )
                {
                    cout << "Cannot update! Pointer is pointing to nullptr!" << endl;
                }
                else
                {
                    cout << "Enter a new name: ";
                    getline( cin, ptrStudent->name );

                    cout << "Enter a new school: ";
                    getline( cin, ptrStudent->school );
                }
            break;

            default:
            done = true;
        }
        cout << endl << endl;
    }
}
