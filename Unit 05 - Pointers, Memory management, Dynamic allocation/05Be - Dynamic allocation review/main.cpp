#include <iostream>
using namespace std;

class DynamicArray
{
    public:
    DynamicArray();
    ~DynamicArray();
    void AllocateMemory();
    void DeallocateMemory();
    void SetValues();
    void DisplayValues();

    private:
    string* m_strPtr;
    int     m_size;
};

DynamicArray::DynamicArray()
{
    m_strPtr = nullptr;
}


DynamicArray::~DynamicArray()
{
    DeallocateMemory();
}

/**
ERROR CHECK: If m_strPtr is NOT to nullptr, throw an exception ("Pointer is already pointing to address!")

Allocate space for a dynamic array of the size passed in, using the m_strPtr pointer.

Use cout and cin to display a message, asking the user to enter a size.
Have the user enter the size into the size variable.

@param  m_strPtr      The pointer to use to allocate memory for an array.
@param  size        The amount of elements to be in the array; pass-by-reference.
*/
void DynamicArray::AllocateMemory()
{
    cout << endl << "ALLOCATE MEMORY" << endl;

    // TODO: Add code here
    if ( m_strPtr != nullptr )
    {
        throw runtime_error( "Pointer is already pointing to address!" );
    }

    cout << "Enter a size for the array: ";
    cin >> m_size;
    m_strPtr = new string[m_size];


    cout << "strPtr is pointing to " << m_strPtr << endl;
}

/**
Check to see if m_strPtr is pointing to nullptr. If it is NOT pointing to nullptr,
then free the memory of m_strPtr and reset m_strPtr to point to nullptr.

@param  m_strPtr      The pointer to use to deallocate memory from.
*/
void DynamicArray::DeallocateMemory()
{
    cout << endl << "DEALLOCATE MEMORY" << endl;

    // TODO: Add code here
    if ( m_strPtr != nullptr )
    {
        delete [] m_strPtr;
    }

    m_strPtr = nullptr;
    cout << "strPtr is pointing to " << m_strPtr << endl;
}

/**
ERROR CHECK: If m_strPtr is pointing to nullptr, throw an exception ("Pointer is null!")

Use a for loop, iterating from 0 to size, and use cout and cin
to ask the user to enter a value for each element of the array
stored at m_strPtr.

@param  m_strPtr      A pointer that is allocated to an array of strings.
@param  size        The size of the array (total amount of elements it can store).
*/
void DynamicArray::SetValues()
{
    cout << endl << "SET VALUES" << endl;

    // TODO: Add code here
    if ( m_strPtr == nullptr)
    {
        cout << "ERROR- suffer.";
    }

    int i = 0;
    for ( i = 0; i < m_size; i++ )
    {
        cout << "Enter a value at " << i << ": ";
        cin >> m_strPtr[i];
    }
}

/**
ERROR CHECK: If m_strPtr is pointing to nullptr, throw an exception ("Pointer is null!")

Use a for loop, iterating from 0 to size, and use cout to output
the INDEX (i) and the VALUE of each element of the array pointed to by m_strPtr.

@param  m_strPtr      A pointer that is allocated to an array of strings.
@param  size        The size of the array (total amount of elements it can store).
*/
void DynamicArray::DisplayValues()
{
    cout << endl << "DISPLAY VALUES" << endl;

    // TODO: Add code here
    int i = 0;
    for ( i = 0; i < m_size; i++ )
    {
        cout << i << "\t" << m_strPtr[i] << endl;
    }
}

// Keep main as-is.
int main()
{
    DynamicArray dynArr;
    dynArr.AllocateMemory();
    dynArr.SetValues();
    dynArr.DisplayValues();
    dynArr.DeallocateMemory();

    return 0;
}
