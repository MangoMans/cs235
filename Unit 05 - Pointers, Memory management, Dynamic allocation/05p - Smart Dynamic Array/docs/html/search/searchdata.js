var indexSectionsWithContent =
{
  0: "acdefghilmnoprstw~",
  1: "lmnpst",
  2: "lmnprst",
  3: "acdefghimnoprstw~",
  4: "cmnst",
  5: "s",
  6: "el",
  7: "cs"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "related",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Friends",
  6: "Macros",
  7: "Pages"
};

