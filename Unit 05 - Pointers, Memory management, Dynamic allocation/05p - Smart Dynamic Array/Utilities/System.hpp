#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <string>
using namespace std;

class System
{
    public:
    static void CreateDirectory( const string& directory, bool relative = true );
    static void Sleep( int delay );
    static void DisplayDirectoryContents( string path );

    private:
    static void CreateRelativeDirectory( const string& directory );
};

#endif
