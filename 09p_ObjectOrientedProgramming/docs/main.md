# Tic Tac Toe program

## Relevant documentation:

* Board
* IPlayer and child classes
    * AIPlayer
    * HumanPlayer
* PlayerManager
* Program

---------------------

## About the program

```

     0     1     2   (x)
0       |     |    
    ---------------
1       |     |    
    ---------------
2       |     |    

(y)
--------------------------------------------------------------------------------
 | Round 1, Bob's turn |
 -----------------------

Enter an x, y position to move to: 


```

This program implements a Tic Tac Toe game using
multiple classes and polymorphism. In this game,
two players are set up, and they can be set up as
either a Human player or an AI player.

---------------------

## Program architecture

![Diagram of the classes](../diagram.png)

The program itself is located in the **Program** class. It handles
most of the interfacing with the players, and passes data between
the PlayerManager and the Board.

The **PlayerManager** stores both the players and provides an interface
to access information *about* those Players, without giving direct
access to the players themselves.

The **IPlayer** base class contains the in-common parts of each player,
with **HumanPlayer** and **AIPlayer** implementing their own versions
of the ChooseMove and GetType functions.

The **Board** class contains a 3x3 array of characters to represent
a Tic Tac Toe board. It also has functionality to update the board,
check if something is already in a cell, and check for a winner.

---------------------

## Testing

When the program first runs, it runs the `BoardTester`'s set of unit tests.
You can use this to validate your board logic in the game.

Remember to check the generated file **test_result.html**, located in your
project directory.

---------------------

## Additional hints

### Board

You can use the unit tests already written to validate
a lot of your logic for the Board class.
    
#### Iterate over the entire board

To iterate over the entire board, you will use a nested for loop.
For instance, to clear all cells in the board, you can write:
    
```
void Board::Clear()
{
    for ( int y = 0; y < BOARD_HEIGHT; y++ )
    {
        for ( int x = 0; x < BOARD_WIDTH; x++ )
        {
            m_board[x][y] = ' ';
        }
    }
}
```
    
    


#### Board win states

In the Board::FindFullLine() function you will write code to detect
all the possible win states. This function will return either:
* 'x' if x has a win state, 
* 'o' if o has a win state, or
* ' ' (A char with a space) if nobody has won. 

**Win states:**

1.                  | 2.                | 3.
--------------------|-------------------|--------------------|
![](../win1.png)    | ![](../win2.png)  | ![](../win3.png)

4.                  | 5.                | 6.
--------------------|-------------------|--------------------|
![](../win4.png)    | ![](../win5.png)  | ![](../win6.png)

7.                  | 8.                
--------------------|-------------------
![](../win7.png)    | ![](../win8.png)  


**How to approach:**

If you can't think of how to investigate win states using loops,
start off just by checking each possible state manually. For example:

```
if ( m_board[0][0] == 'x' && m_board[1][0] == 'x' && m_board[2][0] == 'x' ) 
    return 'x';
```

This corresponds to win state #1, with 'x' (not 'o').

Perhaps as you implement each of the win states, you will begin to see a
pattern emerge, and you can add in some loops in order to write fewer lines.

As long as your logic works (e.g., the unit tests pass), no points will be taken off.


---------------------

## Example output

**Setup:**

```
--------------------------------------------------------------------------------
 | SETUP |
 ---------


PLAYER 1
 1.	Human
 2.	AI

 >> 1

 Enter player name:

 >> PlayerH


PLAYER 2
 1.	Human
 2.	AI

 >> 2

 Enter player name:

 >> PlayerAI
```

**Human round:**

```
     0     1     2   (x)
0       |     |    
    ---------------
1       |     |    
    ---------------
2       |     |    

(y)
--------------------------------------------------------------------------------
 | Round 1, PlayerH's turn |
 ---------------------------

Enter an x, y position to move to: 0 0
     0     1     2   (x)
0   [ ] |     |    
    ---------------
1       |     |    
    ---------------
2       |     |    

(y)

Is this the location you want to place your piece?
 1.	Continue
 2.	Re-enter

 >> 1
```

**AI Round:**

The AI will select its position at random.

```
     0     1     2   (x)
0    x  |     |    
    ---------------
1       |  o  |    
    ---------------
2       |     |    

(y)
```




<!--

Column                      | Description
----------------------------|--------------------------------------------------------------------------------
**Test set**                | What function is being tested?
**Test**                    | One specific test, what is this test trying to do?
**Prerequisite functions**  | This test set may require another function to have already been implemented.
**Pass/fail**               | Did the test pass or fail?
**Expected output**         | What the test was expecting as a result.
**Actual output**           | What was actually discovered as the result.
**Comments**                | Some tests have comments to help you diagnose errors.

-->
