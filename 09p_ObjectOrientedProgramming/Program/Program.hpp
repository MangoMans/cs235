#ifndef _PROGRAM_HPP
#define _PROGRAM_HPP

#include "../Players/PlayerManager.hpp"
#include "../Board/Board.hpp"

class Program
{
    public:
    void Run();

    private:
    PlayerManager m_playerManager;
    Board m_board;
    bool m_done;
    int m_round;

    void Init();
    void GameLoop();
    void DisplayResult();
};

#endif
