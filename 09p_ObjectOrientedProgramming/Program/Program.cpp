#include "Program.hpp"

#include "../Utilities/Menu.hpp"
#include "../Utilities/StringUtil.hpp"

/**
Runs the program. Begins by calling Init(), then calling the GameLoop().
*/
void Program::Run()
{
    Init();
    GameLoop();
    DisplayResult();
}

/**
Create a loop that continues while the m_done boolean is false. Within the loop, one round is executed. This includes:

1. Draw the game board. -Done
2. Display the round # and whose turn it is.
3. Have the current player select an (x, y) position (via the m_playerManager function GetCurrentPlayerChoice.)
4. If the cell isn't already taken, then place the player's symbol at this position (via the m_playerManager function UpdateCell.)
5. If someone has a full line, set m_done to true. (Check via m_playerManager function FindFullLine.)
6. Change to next player (via the m_playerManager function NextPlayer.)
*/
void Program::GameLoop()
{
    bool m_done = false;

    while ( m_done = false )
    {
        m_board.Draw();


    }
    // TODO: Implement the game loop
}

/**
Displays the final state of the game board
and displays the message "Player 1 wins" or "Player 2 wins" based
on which player won (find out via the FindFullLine function.)
*/
void Program::DisplayResult()
{
    // TODO: Display the results (final board state, and who won.)
}

/**
Initializes the program.
Calls the player manager's SetupPlayers function,
calls the board's Clear function,
initializes m_done to false,
and initializes m_round to 1.
*/
void Program::Init()
{
    Menu::Header( "SETUP" );
    m_playerManager.SetupPlayers();
    m_board.Clear();
    m_done = false;
    m_round = 1;
}
