#ifndef _PLAYER_HPP
#define _PLAYER_HPP

#include <string>
using namespace std;

class IPlayer
{
    public:
    IPlayer()           {}
    virtual ~IPlayer()  {}

    virtual void ChooseMove( int& x, int& y ) = 0;
    virtual string GetType() = 0;

    void SetName( string name );
    string GetName();

    void SetSymbol( char value );
    char GetSymbol();

    protected:
    string m_name;
    char  m_symbol;
};

class HumanPlayer : public IPlayer
{
    public:
    HumanPlayer()           {}
    virtual ~HumanPlayer()  {}

    virtual void ChooseMove( int& x, int& y );
    virtual string GetType();
};

class AIPlayer : public IPlayer
{
    public:
    AIPlayer()          {}
    virtual ~AIPlayer() {}

    virtual void ChooseMove( int& x, int& y );
    virtual string GetType();
};

#endif
