#include "PlayerManager.hpp"

#include "../Utilities/Menu.hpp"

#include <iostream>
#include <string>
using namespace std;

/**
Initializes the PlayerManager by setting all of the pointers in
m_playerPtrs to nullptr.
*/
PlayerManager::PlayerManager()
{
    for ( int i = 0; i < 2; i++ )
    {
        m_playerPtrs[i] = nullptr;
    }
}

/**
Cleans up PlayerManager by checking all pointers in
m_playerPtrs, if any are not nullptr, then delete that memory.
*/
PlayerManager::~PlayerManager()
{
    for ( int i = 0; i < 2; i++ )
    {
        if ( m_playerPtrs[i] != nullptr )
        {
            delete m_playerPtrs[i];
        }
    }
}

/**
Player setup - For each player, ask whether each should be
Human or AI and initialize the corresponding element of m_playerPtrs
to the appropriate subclass of IPlayer (HumanPlayer or AIPlayer).

Then, also ask for the player's name and set via the IPlayer SetName function.

Lastly, set each player's symbol via the IPlayer SetSymbol function. Player 0 should
have 'x' and Player 1 should have 'o'.

Initialize m_currentPlayer to 0.
*/
void PlayerManager::SetupPlayers()
{
    int playerChoice;
    cout << "Chose player One." << endl;
    cout << "0. AI           1. Human" << endl;
    cin >> playerChoice;

    if ( playerChoice == 0 )
    {
        m_playerPtrs[0] = new AIPlayer;
    }

    if ( playerChoice == 1 )
    {
        m_playerPtrs[0]  = new HumanPlayer;
    }

    string name;
    cout << "Enter your name: ";
    cin >> name;

    m_playerPtrs[0]->SetName(name);
    m_playerPtrs[0]->SetSymbol('x');




    cout << "Chose player Two." << endl;
    cout << "0. AI           1. Human" << endl;
    cin >> playerChoice;

    if ( playerChoice == 0 )
    {
        m_playerPtrs[1] = new AIPlayer;
    }

    if ( playerChoice == 1 )
    {
        m_playerPtrs[1]  = new HumanPlayer;
    }

    string nameTwo;
    cout << "Enter your name: ";
    cin >> nameTwo;

    m_playerPtrs[0]->SetName(nameTwo);
    m_playerPtrs[0]->SetSymbol('o');

    m_currentPlayer = 0;
}

/**
Returns the name of the current player via the IPlayer family GetName function.

The current player pointer can be accessed via
`m_playerPtrs[m_currentPlayer]`

@return     The name of the current player.
*/
 string PlayerManager::GetCurrentPlayerName()
{
    return m_playerPtrs[ m_currentPlayer ]->GetName();
}

/**
Gets the move choice of the current player via the IPlayer family ChooseMove function.

The current player pointerr can be accessed via
`m_playerPtrs[m_currentPlayer]`

@param  x       An int& variable to return the player's choice for x coordinate.
@param  y       An int& variable to return the player's choice for y coordinate.
*/
void PlayerManager::GetCurrentPlayerChoice( int& x, int& y )
{
    return m_playerPtrs[ m_currentPlayer ]->ChooseMove(x, y);
}

/**
Gets the symbol associated with the current player via the IPlayer family GetSymbol function.

The current player pointerr can be accessed via
`m_playerPtrs[m_currentPlayer]`

@return     The symbol associated with the current player.
*/
char PlayerManager::GetCurrentPlayerSymbol()
{
    return m_playerPtrs[ m_currentPlayer ]->GetSymbol();
}

/**
Gets the type of the current player via the IPlayer family GetType function.

The current player pointerr can be accessed via
`m_playerPtrs[m_currentPlayer]`

@return     The type associated with the current player.
*/
string PlayerManager::GetCurrentPlayerType()
{
    return m_playerPtrs[ m_currentPlayer ]->GetType();
}

/**
Changes the value of m_currentPlayer between 0 and 1.
*/
void PlayerManager::NextPlayer()
{
    if ( m_currentPlayer = 0)
    {
        m_currentPlayer = 1;
    }
    else
    {
        m_currentPlayer = 0;
    }
}
