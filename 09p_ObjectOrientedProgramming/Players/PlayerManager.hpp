#ifndef _PLAYER_MANAGER_HPP
#define _PLAYER_MANAGER_HPP

#include "Player.hpp"

class PlayerManager
{
    public:
    PlayerManager();
    ~PlayerManager();

    void SetupPlayers();

    string GetCurrentPlayerName();
    void GetCurrentPlayerChoice( int& x, int& y );
    char GetCurrentPlayerSymbol();
    string GetCurrentPlayerType();
    void NextPlayer();

    private:
    IPlayer* m_playerPtrs[2];
    int m_currentPlayer;
};

#endif
