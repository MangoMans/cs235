#include "Player.hpp"

#include <iostream>
#include <cstdlib>
using namespace std;

/**
Sets the player's name
*/
void IPlayer::SetName( string name )
{
    m_name = name;
}

/**
Returns the player's name
*/
string IPlayer::GetName()
{
    return m_name;
}

/**
Sets the player's symbol ('x' or 'o')
*/
void IPlayer::SetSymbol( char value )
{
    m_symbol = value;
}

/**
Gets the player's symbol
*/
char IPlayer::GetSymbol()
{
    return m_symbol;
}

/**
Returns what type of player this is - in this case, "Human".
*/
string HumanPlayer::GetType()
{
    return "Human";
}

/**
HumanPlayer's choose move function. This function should
ask the user to enter x and y values, and make sure they're
within a valid range.

@param  x       The x coordinate the player wants to update
@param  y       The y coordinate the player wants to update
*/
void HumanPlayer::ChooseMove( int& x, int& y )
{
    // TODO: Have the user enter an x, y coordinate.
    // TODO: Ensure that the x and y values are between [0, 2].
}

/**
Returns what type of player this is - in this case, "AI".
*/
string AIPlayer::GetType()
{
    return "AI";
}

/**
AIPlayer's choose move function. This function should
select random x and y values between 0 and 2 for both.
*/
void AIPlayer::ChooseMove( int& x, int& y )
{
    x = rand() % 3;
    y = rand() % 3;
}

