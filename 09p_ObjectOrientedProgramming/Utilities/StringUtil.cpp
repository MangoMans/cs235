#include "StringUtil.hpp"

#include <string>
#include <sstream>
using namespace std;

int StringUtil::StringToInt( const string& str )
{
    istringstream ss( str );
    int val;
    ss >> val;
    return val;
}

string StringUtil::BoolToString( bool value )
{
    return ( value ) ? "true" : "false";
}

string StringUtil::ToUpper( const string& val )
{
    string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += toupper( val[i] );
    }
    return upper;
}

string StringUtil::ToLower( const string& val )
{
    string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += tolower( val[i] );
    }
    return upper;
}

string StringUtil::ColumnText( int colWidth, const string& text )
{
    string adjusted = text;
    for ( unsigned int i = 0; i < colWidth - text.size(); i++ )
    {
        adjusted += " ";
    }
    return adjusted;
}

bool StringUtil::Contains( string haystack, string needle )
{
    return ( haystack.find( needle ) != string::npos );
}
