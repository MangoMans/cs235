#ifndef _TESTER_BASE_HPP
#define _TESTER_BASE_HPP

#include <string>
#include <list>
#include <vector>
#include <functional>
#include <fstream>
using namespace std;

struct TestListItem;

class TesterBase
{
    public:
    TesterBase();
    TesterBase( string outputPath );
    virtual ~TesterBase();

    void Open( string outputPath );
    void Close();

    void Start();
    void AddTest( const TestListItem& test );

    protected:
    void OutputHeader();
    void OutputFooter();
    void TestAll();

    list<TestListItem> m_tests;
    int m_totalTestCount;
    int m_totalTestPass;

    // Per test...:
    void StartTestSet( string name, const vector<string>& prereqs );
    void FinishTestSet();
    void FinishTest();
    void StartTest( string description );
    void TestFail();
    void TestFail( string message );
    void TestPass();
    int TestResult();
    void PrereqTest_Success( string functionName );
    int PrereqTest_Abort( string functionName );

    int m_subtest_totalTests;
    int m_subtest_totalPasses;
    string m_subtest_name;


    // For the HTML write-out...
    void WriteoutRow();
    void WriteoutSummary();

    void Set_TestSet( string value );
    void Set_TestName( string value );
    void Set_TestPrerequisites( const vector<string>& prereqs );
    void Set_Result( bool passed );
    void Set_ExpectedOutput( string variable);
    void Set_ExpectedOutput( string variable, string value );
    void Set_ExpectedOutput( string variable, int value );
    void Set_ExpectedOutput( string variable, char value );
    void Set_ExpectedOutput( string variable, bool value );
    void Set_ActualOutput( string variable );
    void Set_ActualOutput( string variable, string value );
    void Set_ActualOutput( string variable, int value );
    void Set_ActualOutput( string variable, char value );
    void Set_ActualOutput( string variable, bool value );
    bool Set_Outputs( string label, string expected, string actual );
    bool Set_Outputs( string label, int expected, int actual );
    bool Set_Outputs( string label, char expected, char actual );
    bool Set_Outputs( string label, bool expected, bool actual );
    void Set_Comments( string value );

    string col_testSet;
    string col_testName;
    string col_prerequisites;
    string col_result;
    string col_expectedOutput;
    string col_actualOutput;
    string col_comments;

    ofstream m_output;

    ifstream m_header;
    ifstream m_footer;

    string m_outputPath;

    bool m_closed;
};

struct TestListItem
{
        string name;
        bool testAggregate;
        function<int()> callFunction;


        TestListItem()
        {
        testAggregate = false;
        }

        TestListItem( const string name, function<int()> callFunction, bool testAggregate = false )
        {
                Setup( name, callFunction, testAggregate );
        }

        void Setup( const string& name, function<int()> callFunction, bool testAggregate = false )
        {
                this->name = name;
                this->callFunction = callFunction;
                this->testAggregate = testAggregate;
        }
};

#endif
