#ifndef _BINARY_SEARCH_TREE_TESTER_HPP
#define _BINARY_SEARCH_TREE_TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "../cuTEST/TesterBase.hpp"
#include "../Utilities/Menu.hpp"
#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/NotImplementedException.hpp"

#include "Board.hpp"

class BoardTester : public TesterBase
{
public:
    BoardTester()
    {
        AddTest(TestListItem("FindFullLine()",      bind(&BoardTester::Test_FindFullLine, this)));
        AddTest(TestListItem("Clear()",             bind(&BoardTester::Test_Clear, this)));
        AddTest(TestListItem("UpdateCell()",        bind(&BoardTester::Test_UpdateCell, this)));
        AddTest(TestListItem("IsCellTaken()",       bind(&BoardTester::Test_IsCellTaken, this)));
    }

    virtual ~BoardTester() { }

private:
    int Test_FindFullLine();
    int Test_Clear();
    int Test_UpdateCell();
    int Test_IsCellTaken();
};

int BoardTester::Test_IsCellTaken()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BoardTester::Test_IsCellTaken", 3 );
    StartTestSet( "Test_IsCellTaken", { } );

    { /* TEST BEGIN ************************************************************/
        StartTest( "1. Check that IsCellTaken returns true when a cell is not ' '." );

        Board board;
        board.m_board[1][1] = 'x';

        if ( !Set_Outputs( "IsCellTaken( 1, 1 )", true, board.IsCellTaken( 1, 1 ) ) ) {
            TestFail();
        } else {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "1. Check that IsCellTaken returns false when a cell is ' '." );

        Board board;
        board.m_board[2][1] = ' ';

        if ( !Set_Outputs( "IsCellTaken( 2, 1 )", false, board.IsCellTaken( 2, 1 ) ) ) {
            TestFail();
        } else {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BoardTester::Test_UpdateCell()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BoardTester::Test_UpdateCell", 3 );
    StartTestSet( "Test_UpdateCell", { } );

    char players[] = { 'x', 'o' };

    int testCounter = 1;
    // Horizontal win test
    for ( int p = 0; p < 2; p++ )
    {
        for ( int y = 0; y < 3; y++ )
        {
            for ( int x = 0; x < 3; x++ )
            {
                { /* TEST BEGIN ************************************************************/
                    StartTest( StringUtil::ToString( testCounter ) + ". Check that cells get updated to " + StringUtil::ToString( players[p] ) + " at [" + StringUtil::ToString(x) + "][" + StringUtil::ToString(y) + "]" );
                    testCounter++;

                    Board board;
                    board.UpdateCell( x, y, players[p] );

                    Set_ExpectedOutput( "m_board[" + StringUtil::ToString(x) + "][" + StringUtil::ToString(y) + "]",
                        "'" + StringUtil::ToString( players[p] ) + "'" );
                    Set_ActualOutput( "m_board[" + StringUtil::ToString(x) + "][" + StringUtil::ToString(y) + "]",
                        "'" + StringUtil::ToString( board.m_board[x][y] ) + "'" );

                    if ( board.m_board[x][y] != players[p] ) {
                        TestFail();
                    } else {
                        TestPass();
                    }

                    FinishTest();
                } /* TEST END **************************************************************/
            }
        }
    }

    FinishTestSet();
    return TestResult();
}

int BoardTester::Test_Clear()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BoardTester::Test_Clear", 3 );
    StartTestSet( "Test_Clear", { } );

    // Check to make sure board gets cleared
    { /* TEST BEGIN ************************************************************/
        StartTest( "1. Check that board gets cleared" );

        Board board;
        board.m_board[0][0] = 'x';
        board.m_board[1][0] = 'o';
        board.m_board[2][0] = 'x';
        board.m_board[0][1] = 'o';
        board.m_board[1][1] = 'x';
        board.m_board[2][1] = 'o';
        board.m_board[0][2] = 'x';
        board.m_board[1][2] = 'o';
        board.m_board[2][2] = 'x';
        board.Clear();

        bool allCorrect = true;
        for ( int y = 0; y < 3; y++ )
        {
            for ( int x = 0; x < 3; x++ )
            {
                Set_ExpectedOutput( "m_board[" + StringUtil::ToString(x) + "][" + StringUtil::ToString(y) + "]",
                    string( "' '" ) );
                Set_ActualOutput( "m_board[" + StringUtil::ToString(x) + "][" + StringUtil::ToString(y) + "]",
                    string( "'" + StringUtil::ToString( board.m_board[x][y] ) + "'" ) );

                if ( board.m_board[x][y] != ' ' )
                {
                    allCorrect = false;
                }
            }
        }

        if ( !allCorrect ) {
            TestFail();
        } else {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BoardTester::Test_FindFullLine()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BoardTester::Test_FindFullLine", 3 );
    StartTestSet( "Test_FindFullLine", { } );
    ostringstream oss;

    char players[] = { 'x', 'o' };

    int testCounter = 1;
    // Horizontal win test
    for ( int p = 0; p < 2; p++ )
    {
        for ( int y = 0; y < 3; y++ )
        {
            { /* TEST BEGIN ************************************************************/
                string player = ""; player += players[p];
                StartTest( StringUtil::ToString( testCounter ) + ". Win state: " + player + " along y = " + StringUtil::ToString( y ) );
                testCounter++;

                Board board;
                board.Clear();
                board.m_board[0][y] = players[p];
                board.m_board[1][y] = players[p];
                board.m_board[2][y] = players[p];

                if ( !Set_Outputs( "FindFullLine()", players[p], board.FindFullLine() ) ) {
                    TestFail();
                } else {
                    TestPass();
                }

                FinishTest();
            } /* TEST END **************************************************************/
        }
    }

    // Vertical win test
    for ( int p = 0; p < 2; p++ )
    {
        for ( int x = 0; x < 3; x++ )
        {
            { /* TEST BEGIN ************************************************************/
                string player = ""; player += players[p];
                StartTest( StringUtil::ToString( testCounter ) + ". Win state: " + player + " along x = " + StringUtil::ToString( x ) );
                testCounter++;

                Board board;
                board.Clear();
                board.m_board[x][0] = players[p];
                board.m_board[x][1] = players[p];
                board.m_board[x][2] = players[p];

                if ( !Set_Outputs( "FindFullLine()", players[p], board.FindFullLine() ) ) {
                    TestFail();
                } else {
                    TestPass();
                }

                FinishTest();
            } /* TEST END **************************************************************/
        }
    }

    // Diagonal win test
    for ( int p = 0; p < 2; p++ )
    {
        { /* TEST BEGIN ************************************************************/
            string player = ""; player += players[p];
            StartTest( StringUtil::ToString( testCounter ) + ". Win state: " + player + " along diagonal 1" );
            testCounter++;

            Board board;
            board.Clear();
            board.m_board[0][0] = players[p];
            board.m_board[1][1] = players[p];
            board.m_board[2][2] = players[p];

            if ( !Set_Outputs( "FindFullLine()", players[p], board.FindFullLine() ) ) {
                TestFail();
            } else {
                TestPass();
            }

            FinishTest();
        } /* TEST END **************************************************************/

        { /* TEST BEGIN ************************************************************/
            string player = ""; player += players[p];
            StartTest( StringUtil::ToString( testCounter ) + ". Win state: " + player + " along diagonal 2" );
            testCounter++;

            Board board;
            board.Clear();
            board.m_board[2][0] = players[p];
            board.m_board[1][1] = players[p];
            board.m_board[0][2] = players[p];

            if ( !Set_Outputs( "FindFullLine()", players[p], board.FindFullLine() ) ) {
                TestFail();
            } else {
                TestPass();
            }

            FinishTest();
        } /* TEST END **************************************************************/
    }

    // Not a win test
    { /* TEST BEGIN ************************************************************/
        StartTest( StringUtil::ToString( testCounter ) + ". Test for not a win state" );
        testCounter++;

        Board board;
        board.Clear();

        if ( !Set_Outputs( "FindFullLine()", ' ', board.FindFullLine() ) ) {
            TestFail();
        } else {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

#endif
