#ifndef _BOARD_HPP
#define _BOARD_HPP

class Board
{
    public:
    Board();

    void Clear();
    void Draw();
    void DrawHighlight( int highlightx, int highlighty );
    void UpdateCell( int x, int y, char symbol );
    bool IsCellTaken( int x, int y );
    char FindFullLine();

    int GetWidth();
    int GetHeight();

    private:
    const int BOARD_WIDTH;
    const int BOARD_HEIGHT;
    char m_board[3][3];

    friend class BoardTester;
};

#endif
