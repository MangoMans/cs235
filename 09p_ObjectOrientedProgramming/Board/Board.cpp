#include "Board.hpp"

#include <iostream>
using namespace std;

/**
Initializes the board.
*/
Board::Board()
    : BOARD_WIDTH( 3 ), BOARD_HEIGHT( 3 )
{
    Clear();
}

/**
Sets all cells of the board to an empty char: ' '
*/
void Board::Clear()
{
    for ( int y = 0; y < BOARD_HEIGHT; y++ )
    {
        for ( int x = 0; x < BOARD_WIDTH; x++ )
        {
            m_board[x][y] = ' ';
        }
    }
    // TODO: Iterate through all the cells and set them all to ' '.
}

/**
Updates the cell at x, y to the symbol given.

@param  x       x coordinate of cell we are updating
@param  y       y coordinate of cell we are updating
@param  symbol  the symbol to place in the cell
*/
void Board::UpdateCell( int x, int y, char symbol )
{
    m_board[x][y] = symbol;
}

/**
Checks to see if the cell at x, y is taken (it is taken if it's not ' ').

@param  x   x coordinate of cell we are checking
@param  y   y coordinate of cell we are checking
*/
bool Board::IsCellTaken( int x, int y )
{
    if (m_board[x][y] != ' ' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
Checks to see if there are any full lines of 'x' or 'o'
on the board, returns 'x' or 'o' for who has a full line,
or ' ' for no full lines.

@return     'x' if x has a win state, 'o' if o has a win state, or ' ' for no win state.
*/
char Board::FindFullLine()
{
    // Horizontal x checks

    if ( m_board[0][0] == 'x' && m_board[1][0] == 'x' && m_board[2][0] == 'x' )
    {
        return 'x';
    }

    if ( m_board[0][1] == 'x' && m_board[1][1] == 'x' && m_board[2][1] == 'x' )
    {
        return 'x';
    }


    if ( m_board[0][2] == 'x' && m_board[1][2] == 'x' && m_board[2][2] == 'x' )
    {
        return 'x';
    }

    // Vertical x checks
    if ( m_board[0][0] == 'x' && m_board[0][1] == 'x' && m_board[0][2] == 'x' )
    {
        return 'x';
    }

    if ( m_board[1][0] == 'x' && m_board[1][1] == 'x' && m_board[1][2] == 'x' )
    {
        return 'x';
    }

    if ( m_board[2][0] == 'x' && m_board[2][1] == 'x' && m_board[2][2] == 'x' )
    {
        return 'x';
    }

    // Diagonal x checks

    if ( m_board[0][0] == 'x' && m_board[1][1] == 'x' && m_board[2][2] == 'x' )
    {
        return 'x';
    }

    if ( m_board[2][0] == 'x' && m_board[1][1] == 'x' && m_board[0][2] == 'x' )
    {
        return 'x';
    }
    //-----------------------------------------------------------------------------

    //Horizontal o checks

    if ( m_board[0][0] == 'o' && m_board[1][0] == 'o' && m_board[2][0] == 'o' )
    {
        return 'o';
    }

    if ( m_board[0][1] == 'o' && m_board[1][1] == 'o' && m_board[2][1] == 'o' )
    {
        return 'o';
    }


    if ( m_board[0][2] == 'o' && m_board[1][2] == 'o' && m_board[2][2] == 'o' )
    {
        return 'o';
    }

    // Vertical o checks

    if ( m_board[0][0] == 'o' && m_board[0][1] == 'o' && m_board[0][2] == 'o' )
    {
        return 'o';
    }

    if ( m_board[1][0] == 'o' && m_board[1][1] == 'o' && m_board[1][2] == 'o' )
    {
        return 'o';
    }

    if ( m_board[2][0] == 'o' && m_board[2][1] == 'o' && m_board[2][2] == 'o' )
    {
        return 'o';
    }

    // Diagonal o checks

    if ( m_board[0][0] == 'o' && m_board[1][1] == 'o' && m_board[2][2] == 'o' )
    {
        return 'o';
    }

    if ( m_board[2][0] == 'o' && m_board[1][1] == 'o' && m_board[0][2] == 'o' )
    {
        return 'o';
    }
    //-----------------------------------------------------------------------------

    // Horizontal ' ' checks

    if ( m_board[0][0] == ' ' && m_board[1][0] == ' ' && m_board[2][0] == ' ' )
    {
        return ' ';
    }

    if ( m_board[0][1] == ' ' && m_board[1][1] == ' ' && m_board[2][1] ==  ' ' )
    {
        return ' ';
    }


    if ( m_board[0][2] == ' ' && m_board[1][2] == ' ' && m_board[2][2] == ' ' )
    {
        return ' ';
    }

    // Vertical ' ' checks
    if ( m_board[0][0] == ' ' && m_board[0][1] == ' ' && m_board[0][2] == ' ' )
    {
        return ' ';
    }

    if ( m_board[1][0] == ' ' && m_board[1][1] == ' ' && m_board[1][2] == ' ' )
    {
        return ' ';
    }

    if ( m_board[2][0] == ' ' && m_board[2][1] == ' ' && m_board[2][2] == ' ' )
    {
        return ' ';
    }

    // Diagonal ' ' checks

    if ( m_board[0][0] == ' ' && m_board[1][1] == ' ' && m_board[2][2] == ' ' )
    {
        return ' ';
    }

    if ( m_board[2][0] == ' ' && m_board[1][1] == ' ' && m_board[0][2] == ' ' )
    {
        return ' ';
    }
}

/**
Return the board width
*/
int Board::GetWidth()
{
    return BOARD_WIDTH;
}

/**
Return the board height
*/
int Board::GetHeight()
{
    return BOARD_HEIGHT;
}

/**
Draw the board, including (x, y) coordinates
*/
void Board::Draw()
{
    // Draw top indices
    cout << "     ";
    for ( int x = 0; x < BOARD_WIDTH; x++ )
    {
        if ( x != 0 ) { cout << "     "; }
        cout << x;
    }
    cout << "   (x)" << endl;

    // Draw the board
    for ( int y = 0; y < BOARD_HEIGHT; y++ )
    {
        if ( y != 0 ) { cout << endl << "    ---------------" << endl; }
        cout << y << "   ";
        for ( int x = 0; x < BOARD_WIDTH; x++ )
        {
            if ( x != 0 ) { cout << " | "; }
            cout << " " << m_board[x][y] << " ";
        }
    }
    cout << endl << endl << "(y)" << endl;
}

/**
Draw the board, highlighting a specific cell.
*/
void Board::DrawHighlight( int highlightx, int highlighty )
{
    // Draw top indices
    cout << "     ";
    for ( int x = 0; x < BOARD_WIDTH; x++ )
    {
        if ( x != 0 ) { cout << "     "; }
        cout << x;
    }
    cout << "   (x)" << endl;

    // Draw the board
    for ( int y = 0; y < BOARD_HEIGHT; y++ )
    {
        if ( y != 0 ) { cout << endl << "    ---------------" << endl; }
        cout << y << "   ";
        for ( int x = 0; x < BOARD_WIDTH; x++ )
        {
            if ( x != 0 ) { cout << " | "; }
            if ( x == highlightx && y == highlighty )
            {
                cout << "[" << m_board[x][y] << "]";
            }
            else
            {
                cout << " " << m_board[x][y] << " ";
            }
        }
    }
    cout << endl << endl << "(y)" << endl;
}
