#include "Program/Program.hpp"
#include "Board/BoardTester.hpp"

#include <cstdlib>
#include <ctime>

int main()
{
    srand( time( NULL ) );

    cout << "Running tests..." << endl;
    BoardTester boardTester;
    boardTester.Start();

    cout << "Starting program..." << endl;
    Program program;
    program.Run();

    return 0;
}
