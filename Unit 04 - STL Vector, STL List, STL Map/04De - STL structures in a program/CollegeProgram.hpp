#ifndef _COLLEGE_PROGRAM_HPP
#define _COLLEGE_PROGRAM_HPP

#include "Student.hpp"

#include <map>
#include <string>
#include <iostream>
using namespace std;

class CollegeProgram
{
    public:
    CollegeProgram( string name );
    void Run();
    void AddStudent();
    void Display();

    private:
    string m_collegeName;
    map<string, Student> m_students;

    void FillTestStudents();
};

CollegeProgram::CollegeProgram( string name )
{
    m_collegeName = name;
    FillTestStudents();
}

void CollegeProgram::Run()
{
    bool done = false;
    int choice;
    while ( !done )
    {
        cout << endl << "-------------------------------------" << endl;
        cout << "College: " << m_collegeName << endl;
        cout << "Total students: " << m_students.size() << endl;
        cout << "-------------------------------------" << endl;
        cout << "MAIN MENU" << endl;
        cout << "1. Add student" << endl;
        cout << "2. View college information" << endl;
        cout << "3. End" << endl;
        cout << endl;
        cout << "Choice: ";
        cin >> choice;

        if ( choice == 1 )
        {
            AddStudent();
        }
        else if ( choice == 2 )
        {
            Display();
        }
        else if ( choice == 3 )
        {
            done = true;
        }
        else
        {
            cout << "Invalid selection." << endl;
        }
    }
}

void CollegeProgram::AddStudent()
{
    string studentId;
    cout << "Enter a student ID: ";
    cin >> studentId;

    Student newStudent;
    m_students[ studentId ].Setup( studentId );
}

void CollegeProgram::Display()
{
    cout << endl;
    cout << "COLLEGE: " << m_collegeName << endl;
    cout << "TOTAL STUDENTS: " << m_students.size() << endl;
    cout << endl;
    for ( auto& student : m_students )
    {
        student.second.Display();
    }
    // TODO: Iterate through m_students, displaying each student via its Display() function.
}

void CollegeProgram::FillTestStudents()
{

    // TODO: Add a few test students
}

#endif
