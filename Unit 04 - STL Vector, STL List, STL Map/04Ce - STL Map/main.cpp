#include <iostream>
#include <map>
#include <string>
using namespace std;

// Map documentation:
// http://www.cplusplus.com/reference/map/map/

void AddItem( map<string, string>& myMap );
void GetItem( map<string, string>& myMap );
void Display( map<string, string>& myMap );
void Clear( map<string, string>& myMap );

int main()
{
    map<string, string> myMap;

    bool done = false;
    while ( !done )
    {
        cout << "---------------------------------------" << endl;
        cout << "MAIN MENU" << endl;
        cout << "Map size: " << myMap.size() << endl << endl;

        cout << "1. Add Item" << endl;
        cout << "2. Get Item" << endl;
        cout << "3. Display" << endl;
        cout << "4. Clear" << endl;
        cout << "5. Exit" << endl;

        int choice;
        cout << ">> ";
        cin >> choice;

        if      ( choice == 1 )     { AddItem( myMap ); }
        else if ( choice == 2 )     { GetItem( myMap ); }
        else if ( choice == 3 )     { Display( myMap ); }
        else if ( choice == 4 )     { Clear( myMap ); }
        else if ( choice == 5 )     { done = true; }
    }
}

/**
http://www.cplusplus.com/reference/map/map/operator[]/
mapped_type& operator[] (const key_type& k);

If k matches the key of an element in the container, the function returns a reference to its mapped value.

If k does not match the key of any element in the container, the function inserts a new element with that
key and returns a reference to its mapped value. Notice that this always increases the container size by
one, even if no mapped value is assigned to the element (the element is constructed using its default constructor).
*/
void AddItem( map<string, string>& myMap )
{
    cout << endl << "PUSH BACK" << endl;
    string key, value;

    cout << "Enter a key: ";
    cin >> key;

    cout << "Enter a value: ";
    cin >> value;

    myMap[key] = value;
    cout << "Added \"" << value << "\" to key \"" << key << "\"" << endl;
}

/**
http://www.cplusplus.com/reference/map/map/find/
iterator find (const key_type& k);
Searches the container for an element with a key equivalent to k and returns an
iterator to it if found, otherwise it returns an iterator to map::end.
*/
void GetItem( map<string, string>& myMap )
{
    cout << endl << "GET ITEM" << endl;
    string key;
    cout << "Enter the key: ";
    cin >> key;

    try
    {
        cout << "Item at " << key << ": " << myMap.at( key ) << endl;
    }
    catch( const out_of_range& ex )
    {
        cout << "Key " << key << " not in map!" << endl;
    }
}

/**
Range-based for loop
*/
void Display( map<string, string>& myMap )
{
    cout << endl << "DISPLAY" << endl;
    for ( auto& element : myMap )
    {
        cout << "key = " << element.first << ", value = " << element.second << endl;
    }
    cout << endl;
}

/**
http://www.cplusplus.com/reference/map/map/clear/
void clear() noexcept;
Removes all elements from the map container (which are destroyed), leaving the container with a size of 0.
*/
void Clear( map<string, string>& myMap )
{
    myMap.clear();
    cout << endl << "CLEAR: Cleared map" << endl;
}

