#include <iostream>
#include <list>
#include <string>
using namespace std;

// List documentation:
// https://www.cplusplus.com/reference/list/list/

void PushFront( list<string>& myList );
void PushBack( list<string>& myList );
void PopFront( list<string>& myList );
void PopBack( list<string>& myList );
void AccessFront( list<string>& myList );
void AccessBack( list<string>& myList );
void Clear( list<string>& myList );
void Display( list<string>& myList );

int main()
{
    list<string> myList;

    bool done = false;
    while ( !done )
    {
        cout << "---------------------------------------" << endl;
        cout << "MAIN MENU" << endl;
        cout << "List size: " << myList.size() << endl << endl;

        cout << "1. Push Front" << endl;
        cout << "2. Push Back" << endl;
        cout << "3. Pop Front" << endl;
        cout << "4. Pop Back" << endl;
        cout << "5. Access Front" << endl;
        cout << "6. Access Back" << endl;
        cout << "7. Clear" << endl;
        cout << "8. Display " << endl;
        cout << "9. Exit" << endl;

        int choice;
        cout << ">> ";
        cin >> choice;

        if      ( choice == 1 )     { PushFront( myList ); }
        else if ( choice == 2 )     { PushBack( myList ); }
        else if ( choice == 3 )     { PopFront( myList ); }
        else if ( choice == 4 )     { PopBack( myList ); }
        else if ( choice == 5 )     { AccessFront( myList ); }
        else if ( choice == 6 )     { AccessBack( myList ); }
        else if ( choice == 7 )     { Clear( myList ); }
        else if ( choice == 8 )     { Display( myList ); }
        else if ( choice == 9 )     { done = true; }
    }
}

/**
https://www.cplusplus.com/reference/list/list/push_front/
void push_front (const value_type& val);
Inserts a new element at the beginning of the list, right before its current first element.
The content of val is copied (or moved) to the inserted element.
*/
void PushFront( list<string>& myList )
{
    cout << endl << "PUSH FRONT" << endl;
    string value;
    cout << "Enter a value to add: ";
    cin >> value;
    myList.push_front( value );
    cout << "Added \"" << value << "\" to the front" << endl;
}

/**
https://www.cplusplus.com/reference/list/list/push_back/
void push_back (const value_type& val);
Adds a new element at the end of the list container, after its current last element.
The content of val is copied (or moved) to the new element.
*/
void PushBack( list<string>& myList )
{
    cout << endl << "PUSH BACK" << endl;
    string value;
    cout << "Enter a value to add: ";
    cin >> value;
    myList.push_back( value );
    cout << "Added \"" << value << "\" to the back" << endl;
}

/**
https://www.cplusplus.com/reference/list/list/pop_front/
void pop_front();
Removes the first element in the list container, effectively reducing its size by one.
*/
void PopFront( list<string>& myList )
{
    myList.pop_front();
    cout << endl << "POPFRONT: Removed the first item" << endl;
}

/**
https://www.cplusplus.com/reference/list/list/pop_back/
void pop_back();
Removes the last element in the list container, effectively reducing the container size by one.
*/
void PopBack( list<string>& myList )
{
    myList.pop_back();
    cout << endl << "POPBACK: Removed the last item" << endl;
}

/**
https://www.cplusplus.com/reference/list/list/front/
reference front();
Returns a reference to the first element in the list container.
*/
void AccessFront( list<string>& myList )
{
    cout << "FRONT: The first item is " << myList.front() << endl;
}

/**
https://www.cplusplus.com/reference/list/list/back/
reference back();
Returns a reference to the last element in the list container.
*/
void AccessBack( list<string>& myList )
{
    cout << "BACK: The last item is " << myList.back() << endl;
}

/**
https://www.cplusplus.com/reference/list/list/clear/
void clear();
Removes all elements from the list container (which are destroyed), and leaving the container with a size of 0.
*/
void Clear( list<string>& myList )
{
    myList.clear();
    cout << endl << "CLEAR: Cleared list" << endl;
}

/**
Range-based for loop
*/
void Display( list<string>& myList )
{
    cout << endl << "DISPLAY" << endl;
    for ( auto& element : myList )
    {
        cout << element << endl;
    }
    cout << endl;
}

