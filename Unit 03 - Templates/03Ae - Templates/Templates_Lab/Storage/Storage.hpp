#ifndef _STORAGE_HPP
#define _STORAGE_HPP

#include <stdexcept>
#include <iomanip>
#include <iostream>
using namespace std;

#include "../Utilities/Menu.hpp"

class Storage
{
    public:
    Storage();

    void Add( string newItem );
    void Display();

    bool IsFull();

    private:
    const int ARRAY_SIZE;
    int m_itemCount;
    string m_arr[20];
};

Storage::Storage()
    : ARRAY_SIZE( 20 )
{
    m_itemCount = 0;
}

bool Storage::IsFull()
{
    return ( m_itemCount == ARRAY_SIZE );
}

void Storage::Add( string newItem )
{
    if ( IsFull() )
    {
        throw runtime_error( "Array is full!" );
    }

    m_arr[ m_itemCount ] = newItem;
    m_itemCount++;
}

void Storage::Display()
{
    Menu::Header( "DISPLAY CONTENTS" );

    const int COL1 = 3;
    const int COL2 = 20;
    const int COL3 = 3;

    cout << right << setw( COL1 ) << "#"
        << setw( COL3 ) << " | " << left
        << left << setw( COL2 ) << "val"
        << right << setw( COL1 ) << "#"
        << setw( COL3 ) << " | "
        << left << setw( COL2 ) << "val"
        << right << setw( COL1 ) << "#"
        << setw( COL3 ) << " | "
        << left << setw( COL2 ) << "val"
        << endl;

    Menu::DrawHorizontalBar( 80, '-' );

    for ( int i = 0; i < m_itemCount; i++ )
    {
        if ( i != 0 && i % 3 == 0 )
        {
            cout << endl;
        }

        cout << left
        << setw( COL1 ) << i
        << setw( COL3 ) << " | "
        << setw( COL2 ) << m_arr[i];
    }

    cout << endl << endl;
}

#endif
